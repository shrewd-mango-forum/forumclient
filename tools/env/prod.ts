import { EnvConfig } from './env-config.interface';

const ProdConfig: EnvConfig = {
	API: 'http://shrewd-mango.azurewebsites.net'
};

export = ProdConfig;

