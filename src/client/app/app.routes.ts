import { Routes, RouterModule } from '@angular/router';
import { threadRoutes } from './thread/thread.routes';
import { topicRoutes } from './topic/topic.routes';

export const routes: Routes = [
	{
    path: '',
    redirectTo: 'topic/',
		pathMatch: 'full'
	},
	...topicRoutes,
	...threadRoutes
];

export const appRouting = RouterModule.forRoot(routes);
