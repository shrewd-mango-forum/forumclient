import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { APP_BASE_HREF, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { appRouting } from './app.routes';

import { ThreadModule } from './thread/thread.module';
import { TopicModule } from './topic/topic.module';
import { SharedModule } from './shared/shared.module';

@NgModule({
  imports: [
		BrowserModule,
		HttpModule,
		appRouting,
		ThreadModule,
		TopicModule,
		SharedModule.forRoot()],
  declarations: [
    AppComponent
  ],
  providers: [
		{
			provide: APP_BASE_HREF,
			useValue: '<%= APP_BASE %>'
  	},
		{
			provide: LocationStrategy,
			useClass: HashLocationStrategy
	}],
  bootstrap: [AppComponent]
})

export class AppModule { }
