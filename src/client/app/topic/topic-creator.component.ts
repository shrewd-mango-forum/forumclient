import {
	Component,
	EventEmitter,
	Input,
	Injectable,
	OnDestroy,
	OnInit,
	Output
} from '@angular/core';
import {
	FormBuilder,
	FormGroup,
	Validators
} from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { TopicService } from '../shared/api/index';
import {
	GetTopicResponseModel,
	PostTopicRequestModel,
	PostTopicResponseModel
} from '../models/topicModels';

@Component({
  moduleId: module.id,
  selector: 'topic-creator',
  templateUrl: 'topic-creator.component.html'
})
@Injectable()
export class TopicCreatorComponent implements OnInit, OnDestroy {
	@Input() parentTopic: GetTopicResponseModel;
	@Output() newTopic = new EventEmitter<PostTopicResponseModel>();
	@Output() cancel = new EventEmitter<void>();
	topicFormGroup: FormGroup;

	formErrors: {[id: string] : string[] } = {
		id: [],
		name: [],
		description: []
	};

	validationMessages: {[fieldName: string] : {[validatorKey: string] : string}} = {
		id: {
			'required' : 'A topic ID is required.',
			'pattern' : PostTopicRequestModel.ID_PATTERN_MESSAGE,
			'maxlength' : `The topic ID must be fewer than ${PostTopicRequestModel.MAX_ID_LENGTH} characters.`,
		},
		name: {
			'required' : 'A topic name is required.',
			'maxlength' : `The topic name must be fewer than ${PostTopicRequestModel.MAX_NAME_LENGTH} characters.`,
		},
		description: {
			'required' : 'A topic description is required.',
		}
	};

	message: String;
	private pending = false;
	private postTopicSubscription: Subscription = new Subscription();
	private formStatusChangeSub: Subscription;
	private nameChangesSub: Subscription;

	constructor(private formBuilder: FormBuilder, private topicService: TopicService) {
	}

	ngOnInit() {
		this.topicFormGroup = this.formBuilder.group({
			'id': ['', [
				Validators.required,
				Validators.maxLength(PostTopicRequestModel.MAX_ID_LENGTH),
				Validators.pattern(PostTopicRequestModel.ID_PATTERN.source)]],
			'name': ['', [Validators.required, Validators.maxLength(PostTopicRequestModel.MAX_NAME_LENGTH)]],
			'description': ['', [Validators.required]]
		});
		this.formStatusChangeSub = this.topicFormGroup.statusChanges.subscribe(
			data => this.onStatusChanged(data)
		);
		this.nameChangesSub = this.topicFormGroup.controls['name'].valueChanges.subscribe(
			value => this.onNameChanged(value)
		);
	}

	onNameChanged(name: string) {
		if (this.topicFormGroup.controls['id'].pristine) {
			this.topicFormGroup.controls['id'].setValue(
				name
				? name.toLowerCase()
					.replace(/[^a-z0-9\s]/g, '')
					.replace(/\s+/g, '-')
				: null
			);
		}
	}

	onStatusChanged(data: any) {
		if (!this.topicFormGroup) {
			return;
		}

		for (const field in this.formErrors) {
			this.formErrors[field] = null;
			const control = this.topicFormGroup.get(field);
			if (control && control.dirty && !control.valid) {
				this.formErrors[field] = Object.keys(control.errors)
					.map(key => this.validationMessages[field][key]);
			}
		}
	}

	onSubmit() {
		const value = this.topicFormGroup.value;
		this.postTopicSubscription = this.topicService.postTopic(
			{
				id: value.id,
				name: value.name,
				description: value.description
			}, this.parentTopic)
			.subscribe(
				newTopic => this.newTopic.emit(newTopic),
				(e: any) => {
					if (e.userMessage) {
						this.pending = false;
						this.message = e.userMessage;
					} else {
						throw e;
					}
				}
			);
		this.pending = true;
		delete this.message;
	}

	ngOnDestroy() {
		this.postTopicSubscription.unsubscribe();
		this.formStatusChangeSub.unsubscribe();
		this.nameChangesSub.unsubscribe();
		this.pending = false;
	}

	isEnabled() {
		return !this.pending;
	}

	onCancel() {
		this.cancel.emit();
	}
}
