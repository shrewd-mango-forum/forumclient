import { Route } from '@angular/router';
import { TopicComponent } from './topic.component';

export const topicRoutes: Route[] = [
  {
    path: 'topic',
    redirectTo: 'topic/'
  },
	{
		path: 'topic/:topicId',
		component: TopicComponent
	}
];
