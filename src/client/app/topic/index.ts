/**
 * This barrel file provides the export for the lazy loaded TopicComponent.
 */
export * from './topic-creator.component';
export * from './topic.component';
export * from './topic.routes';
