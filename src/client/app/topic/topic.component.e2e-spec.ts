import { ApiHelper, AuthHelper, Identity, PathHelper, adminIdentity, loginSelectors, topicSelectors } from '../../e2e-helpers/';
import { GetTopicResponseModel, PostTopicRequestModel } from '../models/';
describe('Topic', () => {

	var testTopic : GetTopicResponseModel;
	const	randomNumeric= (length: number) => Math.floor(Math.random() * Math.pow(10, length));

	beforeEach(() => {
		const key = `TestTopic${randomNumeric(5)}`;
		const postTopic: PostTopicRequestModel = {
			id: key,
			name: `${key} Topic`,
			description: `Description of ${key}`
		};
		ApiHelper.postTopic(`${PathHelper.apiLocation}/api/Topic`, postTopic)
			.then(result => ApiHelper.getTopic(result.actions.getTopicUrl))
			.then(result => testTopic = result);
		browser.get('#/topic');
	});

	afterEach(() => {
		ApiHelper.deleteTopic(testTopic.actions.deleteTopicUrl);
	});

  it('should list the topic under the root topic', () => {
    expect(topicSelectors.childTopicLink(testTopic.name).isDisplayed())
			.toBe(true);
  });

	it('should navigate to the topic from the root topic', () => {
		topicSelectors.childTopicLink(testTopic.name).click();

		expect(element(by.css('.topic-name')).getText()).toEqual(testTopic.name);
		expect(element(by.css('.topic-description')).getText()).toEqual(testTopic.description);
	});

	it('should reveal the "New Topic" button after I log in as an admin', () => {
		expect(element(by.buttonText('New Topic')).isPresent()).not.toBe(true);
		loginSelectors.usernameInput().sendKeys(adminIdentity.username);
		loginSelectors.passwordInput().sendKeys(adminIdentity.password);
		loginSelectors.loginButton().click();

		expect(element(by.buttonText('New Topic')).isDisplayed()).toBe(true);
	});

	it('should show a message and link to root topic when opening a topic that does not exist', () => {
		expect(topicSelectors.error().isPresent()).toBeFalsy();
		browser.get('#/topic/does-not-exist');
		expect(topicSelectors.error().getText()).toContain('does-not-exist');
	});

	it('should not have a parent topic link when viewing the root topic', () => {
		expect(topicSelectors.parentTopicLink().isPresent()).toBeFalsy();
	});

	describe('when logged in as a regular user and viewing a topic under root', () => {
		let identity: Identity;
		beforeAll(() => {
			identity = {
				username: `User${randomNumeric(5)}`,
				password: 'my-secret'
			};
			AuthHelper.registerNewUser(identity);
		});
		beforeEach(() => {
			topicSelectors.childTopicLink(testTopic.id)
				.click();
			loginSelectors.usernameInput().sendKeys(identity.username);
			loginSelectors.passwordInput().sendKeys(identity.password);
			loginSelectors.loginButton().click();
		});

		it('should show the "New Thread" button', () => {
			expect(element(by.buttonText('New Thread')).isDisplayed()).toBe(true);
		});

		it('should not have a "Delete Topic" button', () => {
			expect(topicSelectors.deleteTopicButton().isPresent()).toBe(false);
		});

		it('should have a link to the root topic', () => {
			topicSelectors.parentTopicLink().click();

			expect(browser.getCurrentUrl()
				.then(url => url.endsWith('/topic/')))
				.toBeTruthy();
		});

		describe('when creating a new thread', () => {
			let newThreadTitle = '6-slice toasters';
			let content = 'Have extra cash for one; any recs?';
			beforeEach(() => {
				element(by.buttonText('New Thread')).click();
			});

			it('should allow creating the new thread', () => {
				element(by.css('.thread-title-input')).sendKeys(newThreadTitle);
				element(by.css('.thread-content-input')).sendKeys(content);
				element(by.cssContainingText('thread-creator button', 'Create')).click();

				expect(element(by.cssContainingText('thread-summary', newThreadTitle))
					.isDisplayed()).toBe(true);
			});

			it('should disable the "Create" button whenever the thread is invalid', () => {
				expect(element(by.buttonText('Create')).isEnabled()).toBe(false);

				element(by.css('.thread-title-input')).sendKeys(newThreadTitle);
				element(by.css('.thread-content-input')).sendKeys(content);
				expect(element(by.buttonText('Create')).isEnabled()).toBe(true);

				element(by.css('.thread-title-input')).clear();
				element(by.css('.thread-title-input')).sendKeys(' ', protractor.Key.BACK_SPACE); //?
				expect(element(by.buttonText('Create')).isEnabled()).toBe(false);
			});
		});
	});

	describe('when viewing a grandchild topic', () => {
		let childTopic: PostTopicRequestModel;

		beforeEach(() => {
			const key = randomNumeric(5);
			childTopic = {
				id: `${testTopic.id}.${key}`,
				name: `Topic About ${key}`,
				description: 'Created via api'
			};
			ApiHelper.postTopic(testTopic.actions.postTopicUrl, childTopic);
			browser.get('#/topic');
			topicSelectors.childTopicLink(testTopic.name).click();
			topicSelectors.childTopicLink(childTopic.name).click();
		});

		it('allows navigation back to the middle topic', () => {
			topicSelectors.parentTopicLink().click();
			expect(topicSelectors.topicName().getText()).toBe(testTopic.name);
		});
	});

	describe('when logged in as an admin', () => {
		beforeEach(() => {
			topicSelectors.childTopicLink(testTopic.id).click();

			loginSelectors.usernameInput().sendKeys(adminIdentity.username);
			loginSelectors.passwordInput().sendKeys(adminIdentity.password);
			loginSelectors.loginButton().click();
		});

		it('should allow me to delete the topic and automatically return to the parent', () => {
			topicSelectors.deleteTopicButton().click();
			topicSelectors.confirmDeleteTopicButton().click();

			expect(browser.getCurrentUrl()).toMatch(/\/topic\/$/);
			expect(topicSelectors.childTopicLink(testTopic.id).isPresent()).toBe(false);
		});

		describe('when creating a topic', () => {
			let newTopic: PostTopicRequestModel;

			beforeEach(() => {
				//given
				const key = randomNumeric(5);
				newTopic = {
					id: `${testTopic.id}.${key}`,
					name: `Topic ${key}`,
					description: `Description of ${key}`
				};
				element(by.buttonText('New Topic')).click();
			});

			it('should allow creating a new topic', () => {
				//when
				element(by.css('.new-topic-name')).sendKeys(newTopic.name);
				element(by.css('.new-topic-description')).sendKeys(newTopic.description);
				element(by.buttonText('Create')).click();

				//then
				const newChildTopic = topicSelectors.childTopicLink(newTopic.name);
				expect(newChildTopic.isDisplayed()).toBe(true);
			});

			it('should display an error when trying to create a duplicate subtopic', () => {
				//given
				element(by.css('.new-topic-name')).sendKeys(newTopic.name);
				element(by.css('.new-topic-id')).clear();
				element(by.css('.new-topic-id')).sendKeys(newTopic.id);
				element(by.css('.new-topic-description')).sendKeys(newTopic.description);
				element(by.buttonText('Create')).click();

				//when
				element(by.buttonText('New Topic')).click();
				element(by.css('.new-topic-name')).sendKeys(newTopic.name);
				element(by.css('.new-topic-id')).clear();
				element(by.css('.new-topic-id')).sendKeys(newTopic.id);
				element(by.css('.new-topic-description')).sendKeys(newTopic.description);
				element(by.buttonText('Create')).click();

				//then
				expect(element(by.css('topic-creator .alert')).isDisplayed()).toBe(true);
				expect(element(by.css('topic-creator .alert')).getText()).toContain(newTopic.id);
				expect(element(by.buttonText('Create')).isEnabled()).toBe(true);
			});

			it('should automatically suggest a topic id while a name is being typed', () => {
				//when
				element(by.css('.new-topic-name')).sendKeys('!!!MY GREAT TOPIC <-   CLICK!!!');
				//then
				expect(element(by.css('topic-creator .new-topic-id')).getAttribute('value'))
					.toEqual('my-great-topic-click');
			});

			it('should disable the "Create" button until new topic is valid', function () {
				expect(element(by.buttonText('Create')).isEnabled()).toBe(false);

				element(by.css('.new-topic-name')).sendKeys(newTopic.name);
				element(by.css('.new-topic-description')).sendKeys(newTopic.description);
				expect(element(by.buttonText('Create')).isEnabled()).toBe(true);

				element(by.css('.new-topic-name')).clear();
				element(by.css('.new-topic-name')).sendKeys(' ', protractor.Key.BACK_SPACE); //?
				expect(element(by.buttonText('Create')).isEnabled()).toBe(false);
				expect(element(by.css('topic-creator .validation-message')).getText())
					.toBe('A topic name is required.');
			});
		});
	});
});
