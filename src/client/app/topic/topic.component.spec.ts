import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import {
	async,
	fakeAsync,
	tick,
	ComponentFixture,
	TestBed,
} from '@angular/core/testing';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { MockableServiceMethod } from '../../test-helpers/index';
import {
	GetTopicResponseModel,
	ThreadSummary,
	TopicActions,
	TopicSummary,
	topicIdToGetTopicUrl
} from '../models/index';
import { AuthService, TopicService } from '../shared/api/index';
import { topicRoutes } from './topic.routes';
import { TopicComponent } from './topic.component';

export function main() {
	describe('Topic component', () => {
		class MockTopicService {
			service = {
				getTopic: (url: string) => this._get.request(url),
				deleteTopic: (url: string) => this._delete.request(url),
			};
			private _get = new MockableServiceMethod('GET');
			private _delete = new MockableServiceMethod('DELETE');

			resolveGet = (url: string, value: GetTopicResponseModel) => this._get.resolveRequest(url, value);
			resolveDelete = (url: string) => this._delete.resolveRequest(url, undefined);
			hasOutstandingRequests = () => this._get.hasOutstandingRequests() && this._delete.hasOutstandingRequests();
		};
		let mockTopicService: MockTopicService;

		let authState: BehaviorSubject<void>;

		const mockTopic : GetTopicResponseModel = {
			id: 'appliances.toasters',
			name: 'Toasters',
			description: 'Toaster enthusiasts only',
			topics: [{
				id: 'appliances.toasters.retro',
				name: 'Retro',
				description: 'Pre-80s toasters',
				getTopicUrl: topicIdToGetTopicUrl('appliances.toasters.retro')
			}],
			threads: [{
				threadUrl: '/toasters/retro/child-thread-1',
				id: 'child-thread-1',
				title: 'Is the Cuisinart Compact Stainless 2-Slice overrated?!?'
			}],
			actions: {
				getTopicUrl: topicIdToGetTopicUrl('appliances.toasters'),
				deleteTopicUrl: '/DELETE-toasters/',
				postTopicUrl: '/POST-toasters/',
				postThreadUrl: '/NEWTHREAD-toasters/'
			},
			parent: {
				id: 'appliances',
				name: 'Appliances',
				getTopicUrl: '/GET-appliances',
				description: 'parent of /toasters'
			}
		};

		beforeEach((done) => {
			mockTopicService = new MockTopicService();

			authState = new BehaviorSubject<void>(null);
			const authService = {
				getLoginStateObservable: () => authState.asObservable()
			};

			TestBed
				.configureTestingModule({
						declarations: [
							TestComponent,
							TopicComponent,
							MockParentTopicComponent,
							MockThreadSummaryComponent,
							MockTopicCreatorComponent,
							MockThreadCreatorComponent],
						imports: [
							RouterTestingModule.withRoutes(topicRoutes)
						],
						providers: [
							{
								provide: TopicService,
								useValue: mockTopicService.service
							},
							{
								provide: AuthService,
								useValue: authService
							}
						]
				})
			.compileComponents().then(done);
		});

		afterEach(() => {
			expect(mockTopicService.hasOutstandingRequests()).toBeFalsy();
		});

		describe('viewing a topic other than root', () => {
			let fixture: ComponentFixture<TestComponent>;
			let router: Router;
			const topicUrl = mockTopic.actions.getTopicUrl;
			const selectors = {
				newThreadButton: () => fixture.nativeElement.querySelector('.btn-new-thread'),
				newTopicButton: () => fixture.nativeElement.querySelector('.btn-new-topic'),
				deleteTopicButton: () => fixture.nativeElement.querySelector('.btn-delete-topic'),
				topicCreator: {
					parentTopicName: () => fixture.nativeElement.querySelector('topic-creator .parent-topic-name'),
					newTopicButton: () => fixture.nativeElement.querySelector('topic-creator .btn-new-topic'),
					cancelButton: () => fixture.nativeElement.querySelector('topic-creator .btn-cancel'),
				},
				threadCreator: {
					parentTopicName: () => fixture.nativeElement.querySelector('thread-creator .parent-topic-name'),
					newThreadButton: () => fixture.nativeElement.querySelector('thread-creator .btn-new-thread'),
					cancelButton: () => fixture.nativeElement.querySelector('thread-creator .btn-cancel'),
				},
				topicDeletion: {
					top: () => fixture.nativeElement.querySelector('.confirm-delete-topic'),
					confirm: () => fixture.nativeElement.querySelector('.confirm-delete-topic .btn-confirm'),
					cancel: () => fixture.nativeElement.querySelector('.confirm-delete-topic .btn-cancel'),
				}
			};

			beforeEach(fakeAsync(() => {
				fixture = TestBed.createComponent(TestComponent);
				router = TestBed.get(Router) as Router;

				router.navigateByUrl(`topic/${mockTopic.id}`);
				tick();
				mockTopicService.resolveGet(topicUrl, mockTopic);
				fixture.autoDetectChanges();
			}));

			it('should display parent topic and child topic summaries', () => {
				Object.entries({
					'.topic-name': mockTopic.name,
					'.topic-description': mockTopic.description,
					'.child-topic-name': mockTopic.topics[0].name,
					'.child-topic-description': mockTopic.topics[0].description
				}).forEach(([selector, expectedValue]) => {
					expect(fixture.nativeElement.querySelector(selector).textContent).toEqual(expectedValue);
				});
			});

			it('should allow navigation to child topics', fakeAsync(() => {
				//given
				const mockChildTopic: GetTopicResponseModel = {
					id: `${mockTopic.id}.retro`,
					name: 'Retro Toasters',
					description: 'toasters created before 1980',
					topics: [],
					threads: [],
					actions: {
						getTopicUrl: '/toasters/retro'
					},
					parent: {
						id: mockTopic.id,
						name: mockTopic.name,
						getTopicUrl: mockTopic.actions.getTopicUrl,
						description: mockTopic.description
					}
				};

				//when
				fixture.nativeElement.querySelector('.child-topic-name').click();
				tick();

				//then
				mockTopicService.resolveGet(mockTopic.topics[0].getTopicUrl, mockChildTopic);
				fixture.detectChanges();
				expect(fixture.nativeElement.querySelector('.topic-name').textContent)
					.toBe(mockChildTopic.name);
			}));

			it('should not initially display the topic-creator or the thread-creator', () => {
				expect(selectors.topicCreator.parentTopicName()).toBeFalsy();
				expect(selectors.threadCreator.parentTopicName()).toBeFalsy();
			});

			const overwriteMockTopicActions = (actions: TopicActions) =>
				Object.assign(
					{},
					mockTopic,
					{actions: Object.assign(
						{},
						mockTopic.actions,
						actions
					)}
				);

			it('should have New Topic button only when topic has PostTopicUrl', () => {
				//given
				expect(selectors.newTopicButton()).toBeTruthy();
				authState.next(undefined);

				//when
				mockTopicService.resolveGet(mockTopic.actions.getTopicUrl,
					overwriteMockTopicActions({postTopicUrl: null}));

				//then
				fixture.detectChanges();
				expect(selectors.newTopicButton()).toBeFalsy();
			});

			it('should have New Thread button only when topic has NewThreadUrl', () => {
				//given
				expect(selectors.newThreadButton()).toBeTruthy();
				authState.next(undefined);

				//when
				mockTopicService.resolveGet(mockTopic.actions.getTopicUrl,
					overwriteMockTopicActions({postThreadUrl: null}));

				//then
				fixture.detectChanges();
				expect(selectors.newThreadButton()).toBeFalsy();
			});

			it('should have Delete Topic button only when topic has DeleteTopicUrl', () => {
				//given
				expect(selectors.deleteTopicButton()).toBeTruthy();
				authState.next(undefined);

				//when
				mockTopicService.resolveGet(mockTopic.actions.getTopicUrl,
					overwriteMockTopicActions({deleteTopicUrl: null}));

				//then
				fixture.detectChanges();
				expect(selectors.deleteTopicButton()).toBeFalsy();
			});

			describe('creating a topic', () => {
				beforeEach(() => {
					selectors.newTopicButton().click();
				});

				it('should pass the topic down to the topic-creator', () => {
					expect(selectors.topicCreator.parentTopicName().textContent)
						.toEqual(mockTopic.name);
				});

				it('should refresh topic and remove topic creator when the topic creator emits a newTopic event', () => {
					selectors.topicCreator.newTopicButton().click();
					expect(selectors.topicCreator.parentTopicName()).toBeFalsy();
					mockTopicService.resolveGet(mockTopic.actions.getTopicUrl, mockTopic);
				});

				it('should remove topic creator when the topic creator emits cancel event', () => {
					selectors.topicCreator.cancelButton().click();
					expect(selectors.topicCreator.parentTopicName()).toBeFalsy();
				});

				it('should remove topic creator when New Thread button is clicked', () => {
					selectors.newThreadButton().click();
					expect(selectors.topicCreator.parentTopicName()).toBeFalsy();
				});
			});

			describe('creating a thread', () => {
				beforeEach(() => {
					selectors.newThreadButton().click();
				});

				it('should pass the topic down to the thread-creator', () => {
					expect(selectors.threadCreator.parentTopicName().textContent)
						.toEqual(mockTopic.name);
				});

				it('should refresh topic and remove thread creator when the thread creator emits a newThread event', () => {
					selectors.threadCreator.newThreadButton().click();
					expect(selectors.threadCreator.parentTopicName()).toBeFalsy();
					mockTopicService.resolveGet(mockTopic.actions.getTopicUrl, mockTopic);
				});

				it('should remove thread creator when the thread creator emits cancel event', () => {
					selectors.threadCreator.cancelButton().click();
					expect(selectors.threadCreator.parentTopicName()).toBeFalsy();
				});

				it('should remove thread creator when New Topic button is clicked', () => {
					selectors.newTopicButton().click();
					expect(selectors.threadCreator.parentTopicName()).toBeFalsy();
				});
			});

			describe('deleting a topic', () => {
				beforeEach(() => {
					selectors.deleteTopicButton().click();
				});

				it('should delete after confirming, then load parent topic', fakeAsync(() => {
					selectors.topicDeletion.confirm().click();
					mockTopicService.resolveDelete(mockTopic.actions.deleteTopicUrl);

					tick();
					expect(router.url).toMatch(`/topic/${mockTopic.parent.id}`);
				}));

				it('allows cancellation', () => {
					selectors.topicDeletion.cancel().click();
					expect(selectors.topicDeletion.top()).toBeFalsy();
				});
			});

			it('should pass the threadSummary down', () => {
				expect(fixture.nativeElement.querySelector('thread-summary').textContent)
					.toBe(mockTopic.threads[0].title);
			});

			it('should refresh when the auth service emits a new LoginState', async(() => {
				authState.next(null);
				mockTopicService.resolveGet(mockTopic.actions.getTopicUrl, mockTopic);
			}));
		});
	});
}

@Component({
	selector: 'test-cmp',
	template: '<router-outlet></router-outlet>'
})
class TestComponent { }

@Component({
	selector: 'thread-summary',
	template: '{{thread.title}}'
})
class MockThreadSummaryComponent {
	@Input() thread: ThreadSummary;
}

@Component({
	selector: 'topic-creator',
	template: `
		<span class="parent-topic-name">{{parentTopic.name}}</span>
		<button (click)="emitNewTopic({})" class="btn-new-topic">New Topic</button>
		<button (click)="onCancel()" class="btn-cancel">Cancel</button>
	`
})
class MockTopicCreatorComponent {
	@Output() newTopic = new EventEmitter();
	@Output() cancel = new EventEmitter();
	@Input() parentTopic: GetTopicResponseModel;
	emitNewTopic(o: any) {
		this.newTopic.emit(o);
	}
	onCancel() {
		this.cancel.emit();
	}
}

@Component({
	selector: 'parent-topic',
	template: `<span>{{topic.name}}</span>`
})
class MockParentTopicComponent {
	@Input() topic: TopicSummary;
}

@Component({
	selector: 'thread-creator',
	template: `
		<span class="parent-topic-name">{{parentTopic.name}}</span>
		<button (click)="emitNewThread()" class="btn-new-thread">New Thread</button>
		<button (click)="onCancel()" class="btn-cancel">Cancel</button>
	`
})
class MockThreadCreatorComponent {
	@Output() newThread = new EventEmitter();
	@Output() cancel = new EventEmitter();
	@Input() parentTopic: GetTopicResponseModel;
	emitNewThread(o: any) {
		this.newThread.emit(o);
	}
	onCancel() {
		this.cancel.emit();
	}
}
