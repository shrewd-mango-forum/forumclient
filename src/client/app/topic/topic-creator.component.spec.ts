import {
	DebugElement,
}
from '@angular/core';

import {
	ComponentFixture,
	TestBed
} from '@angular/core/testing';

import {
	ReactiveFormsModule
} from '@angular/forms';

import {
	By
} from '@angular/platform-browser';

import { MockableServiceMethod } from '../../test-helpers/index';

import {
	GetTopicResponseModel,
	PostTopicRequestModel,
	PostTopicResponseModel,
	topicIdToGetTopicUrl
} from '../models/index';
import { TopicService } from '../shared/api/index';
import { TopicCreatorComponent } from './topic-creator.component';

export function main() {
	describe('Topic creator component', () => {
		class MockTopicService {
			service = {
				postTopic: (newTopic: PostTopicRequestModel, parentTopic: GetTopicResponseModel) =>
					this._post.request(newTopic.id)
			};
			_post = new MockableServiceMethod<PostTopicResponseModel>('POST');
			resolveRequest(newTopic: PostTopicRequestModel, returnValue: PostTopicResponseModel) {
				return this._post.resolveRequest(newTopic.id, returnValue);
			}
			failRequest(newTopic: PostTopicRequestModel, error: any) {
				return this._post.failRequest(newTopic.id, error);
			}
			hasOutstandingRequests() {
				return this._post.hasOutstandingRequests();
			}
		};

		let mockTopicService: MockTopicService;
		let fixture: ComponentFixture<TopicCreatorComponent>;
		let component: TopicCreatorComponent;
		let debugElement: DebugElement;
		let element: HTMLElement;

		const selectors = {
			topicNameInput: () => element.querySelector('.new-topic-name') as HTMLInputElement,
			topicIdInput: () => element.querySelector('.new-topic-id') as HTMLInputElement,
			topicDescriptionInput: () => element.querySelector('.new-topic-description') as HTMLTextAreaElement,
			submitButton: () => element.querySelector('button[type=submit]') as HTMLButtonElement,
			errorMessage: () => element.querySelector('.alert'),
		};

		const parentTopic : GetTopicResponseModel = {
			id: 'appliances.toasters',
			name: 'Toasters',
			description: 'Toaster enthusiasts only',
			topics: [],
			threads: [],
			actions: {
				getTopicUrl: topicIdToGetTopicUrl('appliances.toasters'),
				postTopicUrl: '/POST-toasters/',
			},
			parent: {
				id: 'appliances',
				name: 'Appliances',
				getTopicUrl: '/GET-appliances',
				description: 'parent of /toasters'
			}
		};

		beforeEach((done) => {
			mockTopicService = new MockTopicService();

			TestBed
				.configureTestingModule({
						declarations: [
							TopicCreatorComponent
						],
						imports: [
							ReactiveFormsModule
						],
						providers: [
							{
								provide: TopicService,
								useValue: mockTopicService.service
							}
						]
				})
				.compileComponents().then(done);
		});

		beforeEach(() => {
			fixture = TestBed.createComponent(TopicCreatorComponent);
			component = fixture.componentInstance;
			debugElement = fixture.debugElement.query(By.css('form'));
			element = debugElement.nativeElement;

			component.parentTopic = parentTopic;
			fixture.autoDetectChanges();
		});

		afterEach(() => {
			expect(mockTopicService.hasOutstandingRequests()).toBeFalsy();
		});

		it('automatically suggests an appropriate topic id', () => {
			const topicName = '!!New Child Topic!!%#';
			const topicId = 'new-child-topic';

			selectors.topicNameInput().value = topicName;
			selectors.topicNameInput().dispatchEvent(new Event('input'));

			expect(selectors.topicIdInput().value).toEqual(topicId);
		});

		it('should post the topic and emit event after inputting name and description', () => {
			//given
			const expectedPost: PostTopicRequestModel = {
				name: 'New Child Topic',
				id: 'new-child-topic',
				description: 'Discussion about new child topics.'
			};

			selectors.topicNameInput().value = expectedPost.name;
			selectors.topicNameInput().dispatchEvent(new Event('input'));

			selectors.topicDescriptionInput().value = expectedPost.description;
			selectors.topicDescriptionInput().dispatchEvent(new Event('input'));

			//when
			selectors.submitButton().click();

			//then
			const mockReturnValue: PostTopicResponseModel = {
				actions: {
					getTopicUrl: '/newly-created-topic'
				}
			};
			const createdTopics: PostTopicResponseModel[] = [];
			component.newTopic.subscribe((topic: PostTopicResponseModel) => createdTopics.push(topic));
			mockTopicService.resolveRequest(expectedPost, mockReturnValue);

			expect(createdTopics).toEqual([mockReturnValue]);
		});

		it('allows creating a topic with special characters in name and description', () => {
			//given
			const expectedPost: PostTopicRequestModel = {
				name: '国际化',
				id: 'guoji-hua',
				description: '国际化'
			};

			selectors.topicNameInput().value = expectedPost.name;
			selectors.topicNameInput().dispatchEvent(new Event('input'));
			selectors.topicIdInput().value = expectedPost.id;
			selectors.topicIdInput().dispatchEvent(new Event('input'));
			selectors.topicDescriptionInput().value = expectedPost.description;
			selectors.topicDescriptionInput().dispatchEvent(new Event('input'));

			//when
			selectors.submitButton().click();

			//then
			const mockReturnValue: PostTopicResponseModel = {
				actions: {
					getTopicUrl: '/newly-created-topic'
				}
			};
			const createdTopics: PostTopicResponseModel[] = [];
			component.newTopic.subscribe((topic: PostTopicResponseModel) => createdTopics.push(topic));
			mockTopicService.resolveRequest(expectedPost, mockReturnValue);

			expect(createdTopics).toEqual([mockReturnValue]);
		});

		it('displays error message when creating topic fails', () => {
			//given
			const expectedPost: PostTopicRequestModel = {
				name: 'New Child Topic',
				id: 'new-child-topic',
				description: 'Discussion about new child topics.'
			};

			selectors.topicNameInput().value = expectedPost.name;
			selectors.topicNameInput().dispatchEvent(new Event('input'));
			selectors.topicDescriptionInput().value = expectedPost.description;
			selectors.topicDescriptionInput().dispatchEvent(new Event('input'));
			selectors.submitButton().click();

			//when
			const mockError = {userMessage: 'Topic limit exceeded.'};
			const createdTopics: PostTopicResponseModel[] = [];
			component.newTopic.subscribe((topic: PostTopicResponseModel) => createdTopics.push(topic));
			mockTopicService.failRequest(expectedPost, mockError);

			//then
			fixture.detectChanges();
			expect(selectors.errorMessage().textContent).toEqual(mockError.userMessage);
		});
	});
}
