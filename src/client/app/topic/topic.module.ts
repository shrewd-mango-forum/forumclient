import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { ApiModule } from '../shared/api/index';
import { SharedComponentsModule } from '../shared/shared-components/index';
import { ThreadModule } from '../thread/index';
import { TopicComponent } from './topic.component';
import { TopicCreatorComponent } from './topic-creator.component';

@NgModule({
    imports: [ApiModule, CommonModule, ReactiveFormsModule, SharedComponentsModule, ThreadModule, RouterModule],
    declarations: [TopicComponent, TopicCreatorComponent],
    exports: []
})

export class TopicModule { }
