import { OnDestroy, Component, Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { AuthService, TopicService } from '../shared/api/index';
import {
	GetTopicResponseModel,
	TopicSummary,
	getRootTopicUrl,
	topicIdToGetTopicUrl
} from '../models/index';

enum TopicComponentStates {
	normal,
	deleting,
	newTopic,
	newThread
}

export const topicCommands = (topic: TopicSummary) => {
	if (!topic.id) {
		return ['topic'];
	} else {
		return ['topic/', topic.id];
	}
};

@Component({
  moduleId: module.id,
  templateUrl: 'topic.component.html',
	styleUrls: ['topic.component.css']
})
@Injectable()
export class TopicComponent implements OnDestroy {
	public topic?: GetTopicResponseModel;
	public topicObservable: Observable<GetTopicResponseModel>;
	public state = TopicComponentStates.normal;
	public topicComponentStates = TopicComponentStates;
	public getTopicError: string = null;

	private getTopicSubscription: Subscription = new Subscription();
	private deleteTopicSubscription: Subscription = new Subscription();

	constructor(
		private activatedRoute: ActivatedRoute,
		private topicService: TopicService,
		private auth: AuthService,
		private router: Router) {
		this.activatedRoute.params.forEach(param => {
			const topicId = param['topicId'];
			const topicUrl = topicId
					? topicIdToGetTopicUrl(topicId)
					: getRootTopicUrl;
			this.refresh(topicUrl);
		});
	}

	ngOnDestroy() {
		this.getTopicSubscription.unsubscribe();
		this.deleteTopicSubscription.unsubscribe();
	}

	newTopic() {
		this.refresh(this.topic.actions.getTopicUrl);
		this.state = TopicComponentStates.normal;
	}

	newThread() {
		this.refresh(this.topic.actions.getTopicUrl);
		this.state = TopicComponentStates.normal;
	}

	setStateNormal() {
		this.state = TopicComponentStates.normal;
	}

	onDelete() {
		this.state = TopicComponentStates.normal;
		this.deleteTopicSubscription.unsubscribe();
		this.deleteTopicSubscription = this.topicService.deleteTopic(this.topic.actions.deleteTopicUrl)
			.subscribe({
				complete: () => this.router.navigate(topicCommands(this.topic.parent))
			});
	}

	private refresh(getTopicUrl: string) {
			this.getTopicError = null;
			this.getTopicSubscription.unsubscribe();
			this.getTopicSubscription =  this.auth.getLoginStateObservable()
					.switchMap(() => this.topicService.getTopic(getTopicUrl))
					.subscribe({
							next: result => {
								this.topic = result;
								if (this.state === TopicComponentStates.newThread && !this.topic.actions.postThreadUrl) {
									this.state = TopicComponentStates.normal;
								} else if (this.state === TopicComponentStates.newTopic && !this.topic.actions.postThreadUrl) {
									this.state = TopicComponentStates.normal;
								}
							},
							error: (e) => {
								this.topic = null;
								this.getTopicError = `Failed to get the topic from ${getTopicUrl}.`;
							}
					});
	}
}
