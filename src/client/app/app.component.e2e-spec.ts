import { Identity, AuthHelper, loginSelectors, registerSelectors } from '../e2e-helpers';

describe('App', () => {

	beforeEach(() => {
		browser.get('/');
	});

	describe('Login component', () => {
		let identity: Identity;
		beforeEach(() => identity = {
			username: `NewUser${Math.floor(Math.random() * Math.pow(10, 5))}`,
			password: 'secretPassword'
		});

		it('should let an already registered user log in', () => {
			//given
			AuthHelper.registerNewUser(identity);

			//when
			loginSelectors.usernameInput().sendKeys(identity.username);
			loginSelectors.passwordInput().sendKeys(identity.password);
			loginSelectors.loginButton().click();

			//then
			browser.sleep(100);
			expect(loginSelectors.logoutButton().isDisplayed()).toBe(true);
		});

		it('should show error when failing to log in', () => {
			//given
			AuthHelper.registerNewUser(identity);

			//when
			loginSelectors.usernameInput().sendKeys(identity.username);
			loginSelectors.passwordInput().sendKeys('wrong password');
			loginSelectors.loginButton().click();

			//then
			browser.sleep(100);
			expect(loginSelectors.alert().getText())
				.toBe('The user name or password is incorrect.');
		});

		describe('registering new user', () => {
			beforeEach(() => {
				loginSelectors.newUserButton().click();
			});

			it('should let a new user register and automatically log in', () => {
				//when
				registerSelectors.usernameInput().sendKeys(identity.username);
				registerSelectors.passwordInput().sendKeys(identity.password);
				registerSelectors.confirmPasswordInput().sendKeys(identity.password);
				registerSelectors.registerButton().click();

				//then
				browser.sleep(100);
				expect(loginSelectors.logoutButton().isDisplayed()).toBe(true);
			});

			it('should show error when failing to register', () => {
				//given
				AuthHelper.registerNewUser(identity);

				//when
				registerSelectors.usernameInput().sendKeys(identity.username);
				registerSelectors.passwordInput().sendKeys(identity.password);
				registerSelectors.confirmPasswordInput().sendKeys(identity.password);
				registerSelectors.registerButton().click();

				//then
				browser.sleep(100);
				expect(registerSelectors.alert().getText())
					.toBe(`Name ${identity.username} is already taken.`);
			});

			it('should validate that the password and confirmation match', () => {
				registerSelectors.usernameInput().sendKeys(identity.username);
				registerSelectors.passwordInput().sendKeys(identity.password);
				registerSelectors.confirmPasswordInput().sendKeys('wrong password');
				expect(registerSelectors.registerButton().isEnabled()).toBe(false);
				expect(registerSelectors.validationMessages()).toEqual([]);

				registerSelectors.passwordInput().click();
				expect(registerSelectors.validationMessages().map(element => element.getText()))
					.toEqual(['Password and confirmation must match.']);

				registerSelectors.confirmPasswordInput().clear();
				registerSelectors.confirmPasswordInput().sendKeys(identity.password);
				expect(registerSelectors.registerButton().isEnabled()).toBe(true);
				expect(registerSelectors.validationMessages()).toEqual([]);
			});
		});
	});
});
