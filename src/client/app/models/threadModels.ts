import { TopicSummary } from './topicModels';

export interface PostThreadRequestModel {
	title: string;
	content: string;
	parentTopicId: string;
}

export interface PostThreadResponseModel {
	id: string;
	actions: ThreadActions;
}

export interface ThreadActions {
	deleteUrl?: string;
	getUrl: string;
	replyUrl?: string;
}

export interface GetThreadResponseModel {
	title: string;
	id: string;
	topic: TopicSummary;
	author: string;
	replies: Array<ThreadReply>;
	actions: ThreadActions;
}

export interface ThreadReply {
	content: string;
	author: string;
	date: Date;
}
