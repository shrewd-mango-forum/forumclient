export class GetTopicResponseModel {
	id: string;
	name: string;
	description: string;
	parent?: TopicSummary;
	topics: Array<TopicSummary>;
	threads: Array<ThreadSummary>;
	actions: TopicActions;
}

export class PostTopicRequestModel {
	public static MAX_ID_LENGTH = 255;
	public static ID_PATTERN = /^[a-zA-Z0-9\-\.)]*$/;
	public static ID_PATTERN_MESSAGE = `The topic ID must comprise only English letters, numerals, '-' and '.'.`;
	public static MAX_NAME_LENGTH = 255;
	id: string;
	name: string;
	description: string;
}

export class PostTopicResponseModel {
	actions: TopicActions;
}

export class ThreadSummary {
	threadUrl: string;
	id: string;
	title: string;
}

export class TopicSummary {
	getTopicUrl: string;
	id: string;
	name: string;
	description: string;
}

export class TopicActions {
	deleteTopicUrl?: string;
	getTopicUrl?: string;
	postTopicUrl?: string;
	postThreadUrl?: string;
}
