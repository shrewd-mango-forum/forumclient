import { Config } from '../shared/config/env.config';

export const getRootTopicUrl = `${Config.API}/api/topic`;

export function threadIdToThreadUrl(threadId: string) {
	return `${Config.API}/api/thread/${threadId}`;
}
export function topicIdToGetTopicUrl(id: string) {
	return `${Config.API}/api/topic/${id}`;
}
