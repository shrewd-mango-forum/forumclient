import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ApiModule } from './api/index';
import { BecomeAdminServiceImpl } from './auth/become-admin.service';
import {
	BecomeAdminService,
	CurrentUserComponent,
	LoginFormComponent,
	RegisterFormComponent,
	RegisterFormService
} from './auth/index';

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, ApiModule, RouterModule, FormsModule],
  declarations: [CurrentUserComponent, LoginFormComponent, RegisterFormComponent],
  exports: [LoginFormComponent, CommonModule, FormsModule, RouterModule]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
				RegisterFormService,
				{
					provide: BecomeAdminService,
					useClass: BecomeAdminServiceImpl
				}
			]
    };
  }
}
