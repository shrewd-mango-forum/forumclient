import { ReflectiveInjector } from '@angular/core';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { RegisterForm, RegisterFormService } from './register-form.service';


export function main() {
  describe('RegisterForm service', () => {

		let registerFormService: RegisterFormService;
		let registerForm: RegisterForm;
		let formControls: {
			username: AbstractControl,
			password: AbstractControl,
			confirmPassword: AbstractControl
		};

    beforeEach(() => {
      let injector = ReflectiveInjector.resolveAndCreate([
				FormBuilder,
        RegisterFormService
      ]);

			registerFormService = injector.get(RegisterFormService);
			registerForm = registerFormService.get();
			formControls = {
				username: registerForm.form.get('username'),
				password: registerForm.form.get('password'),
				confirmPassword: registerForm.form.get('confirmPassword')
			};
    });

		describe('password confirmation field', () => {
			it('sets required error, not match error, when confirmation isn\'t yet set', () => {
				formControls.password.setValue('valid password');
				expect(Object.keys(formControls.confirmPassword.errors)).toContain('required');
				expect(Object.keys(formControls.confirmPassword.errors)).not.toContain('match');
			});

			it('sets and unsets "match" error as password is updated', () => {
				const confirmPassword = 'correct password';
				formControls.confirmPassword.setValue(confirmPassword);
				formControls.password.setValue(confirmPassword);
				expect(formControls.confirmPassword.errors).toBeFalsy();
				expect(registerForm.getErrorMessages().confirmPassword).toBeFalsy();
				formControls.password.setValue('wrong password');
				expect(Object.keys(formControls.confirmPassword.errors)).toContain('match');
				expect(registerForm.getErrorMessages().confirmPassword.length).toBe(1);
				formControls.password.setValue(confirmPassword);
				expect(formControls.confirmPassword.errors).toBeFalsy();
				expect(registerForm.getErrorMessages().confirmPassword).toBeFalsy();
			});

			it('sets and unsets "match" error as password confirmation is updated', () => {
				const password = 'correct password';
				formControls.password.setValue(password);
				formControls.confirmPassword.setValue(password);
				expect(formControls.confirmPassword.errors).toBeFalsy();
				expect(registerForm.getErrorMessages().confirmPassword).toBeFalsy();
				formControls.confirmPassword.setValue('wrong password');
				expect(Object.keys(formControls.confirmPassword.errors)).toContain('match');
				expect(registerForm.getErrorMessages().confirmPassword.length).toBe(1);
				formControls.confirmPassword.setValue(password);
				expect(formControls.confirmPassword.errors).toBeFalsy();
				expect(registerForm.getErrorMessages().confirmPassword).toBeFalsy();
			});
		});
  });
}
