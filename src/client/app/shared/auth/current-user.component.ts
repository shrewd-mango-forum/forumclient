import { Component } from '@angular/core';

import { LoginState, AuthService } from '../api/index';
import { BecomeAdminService } from './become-admin.service';

enum CurrentUserComponentStates {
	normal,
	becomeAdmin,
}

@Component({
	selector: 'forum-current-user',
	templateUrl: 'current-user.component.html',
	moduleId: module.id,
})
export class CurrentUserComponent {

	public state = CurrentUserComponentStates.normal;
	public states = CurrentUserComponentStates;

	private canBecomeAdmin: boolean = false;
	private loginState: LoginState = null;

	constructor(private authService: AuthService, private becomeAdminService: BecomeAdminService) {
		authService.getLoginStateObservable().subscribe({
			next: loginState => this.loginState = loginState
		});
		this.becomeAdminService.canBecomeAdmin().subscribe({
			next: value => this.canBecomeAdmin = value
		});
	}

	onBecomeAdmin() {
		this.state = CurrentUserComponentStates.becomeAdmin;
	}

	onCancelBecomeAdmin() {
		this.state = CurrentUserComponentStates.normal;
	}

	onConfirmBecomeAdmin() {
		this.becomeAdminService.becomeAdmin()
			.subscribe({
				/* unfortunately, the auth token doesn't have the role, even though the token's user does. */
				next: () => this.authService.logout()
			});
	}

	logout() {
		this.authService.logout();
	}
}
