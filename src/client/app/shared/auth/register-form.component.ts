import { Component, EventEmitter,  Injectable, OnInit, Output, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';

import { AuthService } from '../api/auth.service';
import { RegisterForm, RegisterFormErrorMessages, RegisterFormService } from './register-form.service';

export class RegisteredEvent {
	username: string;
	password: string;
}

@Component({
  moduleId: module.id,
  selector: 'forum-register-form',
  templateUrl: 'register-form.component.html'
})
@Injectable()
export class RegisterFormComponent implements OnInit, OnDestroy {
	@Output() registered = new EventEmitter<RegisteredEvent>();
	@Output() close = new EventEmitter<void>();
	formGroup: FormGroup;
	form: RegisterForm;
	formErrors: () => RegisterFormErrorMessages;
	statusChangesSubscription: Subscription;

	registerError: String;
	registerSubscription: Subscription = null;

	constructor(private formService: RegisterFormService, private authService: AuthService) {
	}

	ngOnInit() {
		this.form = this.formService.get();
		this.formGroup = this.form.form;
		this.formGroup.reset();
		this.formErrors = () => this.form.getErrorMessages();
	}

	ngOnDestroy() {
		if (this.registerSubscription) {
			this.registerSubscription.unsubscribe();
		}
	}

	onSubmit() {
		this.registerError = null;
		const identity = {
			username: this.formGroup.value.username,
			password: this.formGroup.value.password
		};
		this.registerSubscription = this.authService
			.register(identity.username, identity.password)
			.subscribe(
				_ => this.registered.next(identity),
				error => {
					this.registerError = error;
					this.registerSubscription = null;
				}
		);
	}

	onClose() {
		this.close.next();
	}
}
