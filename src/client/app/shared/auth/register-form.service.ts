import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';

const passwordMinLength = 6;

export class RegisterFormErrorMessages {
	username: string[] = null;
	password: string[] = null;
	confirmPassword: string[] = null;
	[key: string]: string[];
}

export interface RegisterForm {
	form: FormGroup;
	getErrorMessages(): RegisterFormErrorMessages;
}

class RegisterFormImpl implements RegisterForm {
	static errorMessages: {[fieldName: string] : {[validatorKey: string] : string}} = {
		username: {
			'required' : 'Username is required.'
		},
		password: {
			'required' : 'Password is required.',
			'minlength' : `Password must be at least ${passwordMinLength} characters long.`,
		},
		confirmPassword: {
			'required' : 'Password confirmation is required.',
			'match' : 'Password and confirmation must match.'
		}
	};

	public form: FormGroup;

	constructor(private formBuilder: FormBuilder) {
		const confirmPasswordValidator: Validator = {
			validate(control) { return null; }
		};
		this.form = this.formBuilder.group({
			'username' : ['', [Validators.required]],
			'password' : ['', [Validators.required, Validators.minLength(passwordMinLength)]],
			'confirmPassword' : ['', [Validators.required, confirmPasswordValidator]]
		});
		confirmPasswordValidator.validate = (control) => {
			/*
			use controls['password'].value instead of form.value because this validator
			can be run before form.value.password is updated
			*/
			if (!control.value) {
				return null;
			} else if (this.form.controls['password'].value === control.value) {
				return null;
			} else {
				return {'match' : true};
			}
		};

		//recheck confirmPassword when password changes
		this.form.controls['password'].valueChanges.subscribe(
			_ => this.form.controls['confirmPassword'].updateValueAndValidity()
		);
	}

	public getErrorMessages() : RegisterFormErrorMessages {
		const retValue = new RegisterFormErrorMessages();
		for (const fieldName in retValue) {
			const control = this.form.get(fieldName);
			if (control && !control.valid) {
				retValue[fieldName] = Object.getOwnPropertyNames(control.errors)
					.map(validatorKey => RegisterFormImpl.errorMessages[fieldName][validatorKey]);
			}
		}
		return retValue;
	}
}

@Injectable()
export class RegisterFormService {

	//use a singleton so the controls['password'].valueChanges subscription doesn't leak.
	private form: RegisterForm;

	constructor(private formBuilder: FormBuilder) {
		this.form = new RegisterFormImpl(formBuilder);
	}

	public get() {
		this.form.form.reset();
		return this.form;
	}
}
