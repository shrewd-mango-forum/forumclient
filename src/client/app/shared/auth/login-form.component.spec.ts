import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { Component, EventEmitter, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AsyncSubject } from 'rxjs/AsyncSubject';

import { AuthService, DemoService, LoginState } from '../api/index';
import { RegisteredEvent } from './register-form.component';
import { LoginFormComponent } from './login-form.component';

export function main() {
  describe('Login form component', () => {

		class MockDemoService {
			service: DemoService = {
				isAvailable: () => this._isAvailable,
				takeAdmin: () => this._takeAdmin,
			};
			private _isAvailable = new Subject<boolean>();
			private _takeAdmin = new Subject<void>();

			resolveIsAvailable = (value: boolean) => this._isAvailable.next(value);
			resolveTakeAdmin = () => this._takeAdmin.next();
		};

		let mockDemoService: MockDemoService;

		class MockAuthService {
			loginState = new BehaviorSubject<LoginState>(null);
			loginParameters: {username: string, password: string};
			service = {
				login: (username: string, password: string) => {
					if (this.loginRequest) {
						throw new Error('Already have one request pending');
					}
					this.loginRequest = new AsyncSubject<void>();
					this.loginParameters = {
						username: username,
						password: password
					};
					return this.loginRequest;
				},
				register: () => { throw new Error('MockAuthService doesn\'t implement register()');},
				getLoginStateObservable: () => {
					return this.loginState.asObservable();
				}
			};

			loginRequest : AsyncSubject<void>;

			expectAndResolveLoginRequest(expectedIdentity: {username: string, password: string}) {
				if (!this.loginRequest) {
					throw new Error('No pending login request');
				}
				expect(expectedIdentity).toEqual(this.loginParameters);
				this.loginRequest.next(null);
				this.loginRequest.complete();
			}

			errorLoginRequest(error: any) {
				if (!this.loginRequest) {
					throw new Error('No pending login request');
				}
				this.loginRequest.error(error);
			}
		};

		let mockAuthService: MockAuthService;

		let fixture: ComponentFixture<LoginFormComponent>;
		const selectors = {
			currentUser: () => fixture.nativeElement.querySelector('forum-current-user'),
			loginForm: {
				form: () => fixture.nativeElement.querySelector('.login-form'),
				usernameInput: () => fixture.nativeElement.querySelector('.login-form .username-input'),
				passwordInput: () => fixture.nativeElement.querySelector('.login-form .password-input'),
				submitButton: () => fixture.nativeElement.querySelector('.login-form button[type=submit]'),
				errorMessage: () => fixture.nativeElement.querySelector('.login-form .alert'),
			},
			newUserButton: () => fixture.nativeElement.querySelector('.new-user-button'),
			registerForm : () => fixture.nativeElement.querySelector('forum-register-form')
		};

    // setting module for testing
    beforeEach(async(() => {
			mockAuthService = new MockAuthService();
			mockDemoService = new MockDemoService();
      TestBed.configureTestingModule({
        imports: [FormsModule],
        declarations: [LoginFormComponent, MockRegisterFormComponent, MockCurrentUserComponent],
        providers: [
          {provide: AuthService, useValue: mockAuthService.service},
					{provide: DemoService, useValue: mockDemoService.service }
        ]
      });
			TestBed.compileComponents().then(() => {
				fixture = TestBed.createComponent(LoginFormComponent);
				fixture.detectChanges();
			});
    }));

		describe('on clicking login button', () => {
			it('displays an error if AuthService.login() emits an error', async(() => {
				//given
				selectors.loginForm.usernameInput().value = 'Bob';
				selectors.loginForm.usernameInput().dispatchEvent(new Event('input'));
				selectors.loginForm.passwordInput().value = 'b';
				selectors.loginForm.passwordInput().dispatchEvent(new Event('input'));
				fixture.detectChanges();

				//when
				selectors.loginForm.submitButton().click();
				let message = 'Wrong password';
				mockAuthService.errorLoginRequest(message);

				//then
				fixture.detectChanges();
				expect(selectors.loginForm.errorMessage().textContent).toBe(message);
			}));
		});

		describe('on clicking the register button', () => {
			beforeEach(() => {
				selectors.newUserButton().click();
				fixture.detectChanges();
			});

			it('hides the login form and displays the register form', () => {
				expect(selectors.loginForm.form()).toBeFalsy();
				expect(selectors.registerForm()).toBeTruthy();
			});

			it('logs in automatically after the register form emits the onRegistered event', () => {
				selectors.registerForm().querySelector('.register-button').dispatchEvent(new Event('click'));
				mockAuthService.expectAndResolveLoginRequest({
						username: MockRegisterFormComponent.registeredEvent.username,
						password: MockRegisterFormComponent.registeredEvent.password}
				);
				const loginState = {username: 'Jane', token: 'jane'};
				mockAuthService.loginState.next(loginState);
				fixture.detectChanges();
				expect(selectors.currentUser()).toBeTruthy();
			});

			it('displays a message if an error occurs after registering but failing to login', () => {
				selectors.registerForm().querySelector('.register-button').dispatchEvent(new Event('click'));
				const message = 'Registration succeeded but login failed';
				mockAuthService.errorLoginRequest(message);
				fixture.detectChanges();
				expect(selectors.loginForm.errorMessage().textContent).toEqual(message);
			});

			it('re-shows the login component if the register form emits a close event', () => {
				selectors.registerForm().querySelector('.close-button').dispatchEvent(new Event('click'));
				fixture.detectChanges();
				expect(selectors.registerForm()).toBeFalsy();
				expect(selectors.loginForm.form()).toBeTruthy();
			});

		});

		describe('on login state changes', () => {
			it('shows the current user after the login state is updated', async(() => {
				expect(selectors.currentUser()).toBeFalsy();

				let user = 'Jack';
				mockAuthService.loginState.next({username: user, token: 'x'});
				fixture.detectChanges();
				expect(selectors.currentUser()).toBeTruthy();
			}));

			it('removes the current user after the login state is cleared', async(() => {
				//given
				mockAuthService.loginState.next({username: 'Jill', token: 'x'});
				fixture.detectChanges();
				expect(selectors.currentUser()).toBeTruthy();

				//when
				mockAuthService.loginState.next(null);
				fixture.detectChanges();

				//then
				expect(selectors.currentUser()).toBeFalsy();
			}));
		});
  });
}

@Component({
	selector: 'forum-register-form',
	template: `
		<button class="close-button" (click)="onClose()">Close</button>
		<button class="register-button" (click)="onRegister()">Register</button>
	`
})
class MockRegisterFormComponent {
	public static registeredEvent = {
		username: 'BrandNewUser',
		password: 'BrandNewSecret'
	};

	@Output() close = new EventEmitter<void>();
	@Output() registered = new EventEmitter<RegisteredEvent>();

	onClose() {
		this.close.next();
	};

	onRegister() {
		this.registered.next(MockRegisterFormComponent.registeredEvent);
	}
}

@Component({
	selector: 'forum-current-user',
	template: `
		You are logged in!
	`
})
class MockCurrentUserComponent {
}
