import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { AccountService, AuthService, DemoService } from '../api/index';

export abstract class BecomeAdminService {
	abstract canBecomeAdmin(): Observable<boolean>;
	abstract becomeAdmin(): Observable<void>;
}

export const adminRoleName = 'administrator';

@Injectable()
export class BecomeAdminServiceImpl extends BecomeAdminService {
	constructor(
		private auth: AuthService,
		private accountService: AccountService,
		private demo: DemoService) {
		super();
	}

	canBecomeAdmin() {
		const isLoggedIn = Observable.of(this.auth.getLoginState() !== null);
		const demoAvailable = this.demo.isAvailable();
		const notAlreadyAdmin = this.accountService.checkRole(adminRoleName)
			.map(isAdmin => !isAdmin);

		//return isLoggedIn && demoAvailable && notAlreadyAdmin
		return(Observable.concat(
			isLoggedIn,
			demoAvailable,
			notAlreadyAdmin))
			.first(value => !value, undefined, true);
	}

	becomeAdmin(): Observable<void> {
		if (this.auth.getLoginState === null) {
			return Observable.throw(new Error('Cannot become admin while not already logged in.'));
		}
		return this.demo.takeAdmin();
	}
}
