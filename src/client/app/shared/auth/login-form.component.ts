import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { AuthService, DemoService } from '../api/index';
import { RegisteredEvent } from './register-form.component';

@Component({
	selector: 'login-form',
	templateUrl: 'login-form.component.html',
	moduleId: module.id,
})
export class LoginFormComponent implements OnDestroy {

	model = {
		username: <string>null,
		password: <string>null,
	};
	newUser: boolean = false;
	loginError: string;
	loggedInUser: string;
	previousSubscription: Subscription = new Subscription();

	private takeAdminFeature: boolean = false;

	constructor(private authService: AuthService, private demoService: DemoService) {
		authService.getLoginStateObservable().subscribe(
			loginState => {
				if (loginState) {
					this.loggedInUser = loginState.username;
				} else {
					this.loggedInUser = null;
				}
			}
		);

		this.demoService.isAvailable().subscribe({
			next: isAvailable => this.takeAdminFeature = isAvailable
		});
	}

	onSubmit() {
		this.loginError = null;
		this.previousSubscription.unsubscribe();
		let password = this.model.password;
		this.model.password = null;

		this.previousSubscription = this.authService
			.login(this.model.username, password)
			.subscribe(
				_ => undefined,
				error => this.loginError = error
			);
	}

	onRegistered(event: RegisteredEvent) {
		this.newUser = false;
		this.previousSubscription.unsubscribe();
		this.previousSubscription = this.authService
			.login(event.username, event.password)
			.subscribe(
				_ => undefined,
				error => this.loginError = error
			);
	}

	logout() {
		this.authService.logout();
	}

	canTakeAdmin() {
		return this.loggedInUser !== null && this.takeAdminFeature;
	}

	ngOnDestroy() {
		this.previousSubscription.unsubscribe();
	}
}
