import { async } from '@angular/core/testing';

import { MockableServiceMethod } from '../../../test-helpers/index';
import {
	AuthService,
	LoginState,
	DemoService,
	AccountService
} from '../api/index';
import { adminRoleName, BecomeAdminServiceImpl } from './become-admin.service';

export function main() {
  describe('BecomeAdmin Service', () => {
		let loginState: LoginState;
    const authService = {
				getLoginState() {
					return loginState;
				}
		} as AuthService;
		let becomeAdminService: BecomeAdminServiceImpl;
		class MockAccountService {
				service: AccountService = {
					checkRole: (roleName) => this._checkRole.request(roleName)
				};
				private _checkRole = new MockableServiceMethod<boolean>('/checkRole/');
				public resolveCheckRole(roleName: string, value: boolean) {this._checkRole.resolveRequest(roleName, value); }
				public hasOutStandingRequests() {return this._checkRole.hasOutstandingRequests(); }
		};
		let mockAccountService: MockAccountService;

		class MockDemoService {
			service: DemoService = {
				isAvailable: () => this._isAvailable.request(''),
				takeAdmin: () => this._takeAdmin.request('')
			};
			private _isAvailable = new MockableServiceMethod<boolean>('/is_available/');
			private _takeAdmin = new MockableServiceMethod<void>('/trust_me/');
			public resolveIsAvailable(value: boolean) {this._isAvailable.resolveRequest('', value);}
			public resolveTakeAdmin() {this._takeAdmin.resolveRequest('', undefined);};
			public hasOutStandingRequests() {
				return this._isAvailable.hasOutstandingRequests() || this._takeAdmin.hasOutstandingRequests();
			}
		};
		let mockDemoService: MockDemoService;

		beforeEach(() => {
			loginState = {username: 'Bob', token: 'Bob\'s token'};
			mockDemoService = new MockDemoService();
			mockAccountService = new MockAccountService();
			becomeAdminService = new BecomeAdminServiceImpl(authService, mockAccountService.service, mockDemoService.service);
		});

		afterEach(() => {
			expect(mockAccountService.hasOutStandingRequests()).toBeFalsy('Expected no outstanding requests for mockAccountService');
			expect(mockDemoService.hasOutStandingRequests()).toBeFalsy('Expected no outstanding requests for mockDemoService');
		});

		describe('canBecomeAdmin', () => {
			it('returns false when not logged in', async(() => {
				loginState = null;
				becomeAdminService.canBecomeAdmin().subscribe(result => expect(result).toBeFalsy());
			}));

			it('returns false when demo api unavailable', async (() => {
				let canBecome: boolean = null;
				becomeAdminService.canBecomeAdmin().subscribe(result => canBecome = result);
				mockDemoService.resolveIsAvailable(false);
				expect(canBecome).toBeFalsy();
			}));

			it('returns false when demo api available, and is already admin', async (() => {
				let canBecome: boolean = null;
				becomeAdminService.canBecomeAdmin().subscribe(result => canBecome = result);
				mockDemoService.resolveIsAvailable(true);
				mockAccountService.resolveCheckRole(adminRoleName, true);
				expect(canBecome).toBeFalsy();
			}));

			it('returns true when demo api available, and not already admin', async (() => {
				let canBecome: boolean = null;
				becomeAdminService.canBecomeAdmin().subscribe(result => canBecome = result);
				mockDemoService.resolveIsAvailable(true);
				mockAccountService.resolveCheckRole(adminRoleName, false);
				expect(canBecome).toBeTruthy();
			}));
		});
  });
}
