import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { ParentTopicComponent } from './index';

@NgModule({
    imports: [CommonModule, RouterModule],
    declarations: [ParentTopicComponent],
    exports: [ParentTopicComponent]
})
export class SharedComponentsModule {
}
