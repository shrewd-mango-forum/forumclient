import { Component, Input } from '@angular/core';
import { TopicSummary } from '../../models/index';

@Component({
	moduleId: module.id,
	templateUrl: 'parent-topic.component.html',
	styleUrls: ['parent-topic.component.css'],
	selector: 'parent-topic'
})
export class ParentTopicComponent {
	@Input() topic: TopicSummary;
	appName = '<%= APP_TITLE %>';
}
