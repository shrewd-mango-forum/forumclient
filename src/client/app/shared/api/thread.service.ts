import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ForumApiService } from './index';

import {
	GetTopicResponseModel,
	GetThreadResponseModel,
	PostReplyRequestModel,
	PostThreadRequestModel,
	PostThreadResponseModel } from '../../models';

interface PostThreadArgs {
	title: string;
	content: string;
	parent: GetTopicResponseModel;
}

interface PostReplyArgs {
	thread: GetThreadResponseModel;
	reply: PostReplyRequestModel;
}

@Injectable()
export class ThreadService {

	constructor(private api: ForumApiService) {
	}

	public postThread(thread: PostThreadArgs): Observable<PostThreadResponseModel> {
		const body: PostThreadRequestModel = {
			title: thread.title,
			content: thread.content,
			parentTopicId: thread.parent.id
		};

		return this.api.post(
				thread.parent.actions.postThreadUrl,
				body
			).map(response => response.json() as PostThreadResponseModel);
	}

	public postReply(data: PostReplyArgs): Observable<void> {
		return this.api.post(
				data.thread.actions.replyUrl,
				data.reply)
			.map(_ => undefined);
	};

	public getThread(getThreadUrl: string): Observable<GetThreadResponseModel> {
		return this.api.get(
				getThreadUrl,
			).map(response => response.json() as GetThreadResponseModel);
	}

	public deleteThread(deleteThreadUrl: string): Observable<void> {
		return this.api.delete(deleteThreadUrl)
			.map(response => undefined);
	}
}
