import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {
	GetTopicResponseModel,
	PostTopicRequestModel,
	PostTopicResponseModel,
	getRootTopicUrl
} from '../../models/index';
import { ForumApiService } from './index';

@Injectable()
export class TopicService {

	constructor(private api: ForumApiService) {
	}

	public getRootTopic(): Observable<GetTopicResponseModel> {
		return this.getTopic(getRootTopicUrl);
	}

	public getTopic(getTopicUrl: string): Observable<GetTopicResponseModel> {
		return this.api.get(getTopicUrl)
			.map(response => response.json() as GetTopicResponseModel);
	}

	public deleteTopic(deleteTopicUrl: string): Observable<void> {
		return this.api.delete(deleteTopicUrl)
			.map(_ => undefined);
	}

	public postTopic(newTopic: PostTopicRequestModel, parentTopic: GetTopicResponseModel)
			: Observable<PostTopicResponseModel> {

		return this.api.post(
				parentTopic.actions.postTopicUrl,
				newTopic
			)
			.catch<Response>((error: any) => {
				if (error.status === 409 /*conflict*/) {
					throw {userMessage: `A topic with an id of '${newTopic.id}' already exists.`};
				}
				console.error(JSON.stringify(error));
				throw error;
			})
			.map(response => response.json() as PostTopicResponseModel);
	}
}
