import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';

import { DemoServiceImpl } from './demo.service';
import { AccountServiceImpl } from './account.service';
import { AccountService, AuthService, DemoService, ForumApiService, ThreadService, TopicService } from './index';

@NgModule({
  imports: [CommonModule, HttpModule],
	providers: [
		AuthService,
		ForumApiService,
		TopicService,
		ThreadService,
		{
			provide: DemoService,
			useClass: DemoServiceImpl
		},
		{
			provide: AccountService,
			useClass: AccountServiceImpl
		}]
})
export class ApiModule {
}
