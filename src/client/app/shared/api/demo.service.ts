import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Config } from '../config/env.config';
import { AuthService } from './auth.service';
import { ForumApiService } from './forum-api.service';


export abstract class DemoService {
	public abstract isAvailable(): Observable<boolean>;
	public abstract takeAdmin(): Observable<void>;
}

@Injectable()
export class DemoServiceImpl extends DemoService {
	constructor(private api: ForumApiService, private auth: AuthService) {
		super();
	}

	public isAvailable(): Observable<boolean> {
		return this.api.head(`${Config.API}/api/demo`)
			.map(response => true)
			.catch(() => Observable.of(false));
	}

	public takeAdmin(): Observable<void> {
		if (this.auth.getLoginState() === null) {
			return Observable.throw(new Error('Can\'t take admin while not logged in'));
		};
		return this.api.post(`${Config.API}/api/demo/trustMe`, {})
			.map(() => undefined);
	}
}
