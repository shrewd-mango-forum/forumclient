import { Injectable } from '@angular/core';
import { Http, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/do';
import 'rxjs/Rx';

import { Config } from '../config/env.config';

export class LoginState {
    username: string;
    token: string;
}

const getTokenUrl = `${Config.API}/Token`;
const registerUrl = `${Config.API}/api/Account/Register`;

@Injectable()
export class AuthService {
    private loginState: BehaviorSubject<LoginState> = new BehaviorSubject(null);

    constructor(private http: Http) {
		}

    login(username: string, password: string): Observable<void> {
        return this.getLoginToken(username, password)
            .map(result => this.loginState.next({username: username, token: result}))
            .catch<void>(error => {
                let description = error['_body']['error_description'];
                if (description) {
                    throw description;
                } else {
                    console.error(error);
                    throw 'Failed to login.';
                }
            });
    }

    register(userName: string, password: string): Observable<void> {
        return this.http.post(registerUrl, {Name: userName, Password: password})
            .catch<void>(error => {
								let body = JSON.parse(error['_body']);
                let modelState = body['modelState'];
                let message = body['message'];
                if (modelState) {
                    throw Object.getOwnPropertyNames(modelState)
                        .reduce((acc: Array<string>, key: string) => acc.concat(modelState[key]), [])
                        .join(' ');
                } else if (message) {
                    throw message;
                } else {
                    console.error(error);
                    throw 'Failed to register.';
                }
            });
    }

    logout() {
        this.loginState.next(null);
    }

    getLoginStateObservable() : Observable<LoginState> {
        return this.loginState.asObservable();
    }

    getLoginState(): LoginState {
        return this.loginState.getValue();
    }

    private getLoginToken(username: string, password: string): Observable<string> {
        let tokenModel: any = {
            'grant_type' : 'password',
            username: username,
            password: password
        };
        let body = Object.keys(tokenModel)
            .map(key => `${key}=${tokenModel[key]}`)
            .join('&');
        return this.http.post(
            getTokenUrl,
            body,
            { responseType: ResponseContentType.Json }
        ).map(result => result.json()['access_token']);
    }
}
