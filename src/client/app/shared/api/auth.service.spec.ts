import { ReflectiveInjector } from '@angular/core';
import { async } from '@angular/core/testing';
import { BaseRequestOptions, ConnectionBackend, Http, Response, ResponseOptions } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';

import { AuthService, LoginState } from './auth.service';

export function main() {
  describe('AuthService Service', () => {
    let authService: AuthService;
    let backend: MockBackend;
		let connection: MockConnection;
		let observedLoginStates: Array<LoginState>;

    beforeEach(() => {

      let injector = ReflectiveInjector.resolveAndCreate([
        AuthService,
        BaseRequestOptions,
        MockBackend,
        {
					provide: Http,
          useFactory: function(backend: ConnectionBackend, defaultOptions: BaseRequestOptions) {
            return new Http( backend, defaultOptions );
          },
          deps: [MockBackend, BaseRequestOptions]
        }
      ]);
      authService = injector.get(AuthService);
      backend = injector.get(MockBackend);

      backend.connections.subscribe((c: MockConnection) => connection = c);

			observedLoginStates = [];
      authService.getLoginStateObservable().subscribe(state => observedLoginStates.push(state));
    });

		afterEach(() => {
			backend.verifyNoPendingRequests();
		});

    it('should already have a null login state', () => {
			expect(authService.getLoginState()).toBe(null);
			expect(observedLoginStates).toEqual([null]);
    });

		it('should emit an event to every subscriber', async(() => {
			authService.getLoginStateObservable().subscribe();
			authService.getLoginStateObservable().subscribe();
		}));

		describe('login()', () => {
			it('should send a form-encoded request', () => {
				//given
				let username = 'Bob', password = 'Bob\'s secret';

				//when
				authService.login(username, password).subscribe();

				//then
				expect(connection.request.getBody()).toEqual(
					`grant_type=password&username=${username}&password=${password}`
				);
			});

			it('should have a login state after logging in successfully', () => {
				//given
				let username = 'Tom';
				let responseObject = {
					access_token: 'top secret'
				};

				//when
				authService.login(username, 'My secret').subscribe();
				connection.mockRespond(new Response(new ResponseOptions({
					status: 200,
					body: JSON.stringify(responseObject)
				})));

				var expectedLoginState = {
					token: responseObject.access_token,
					username: username
				};

				expect(authService.getLoginState()).toEqual(expectedLoginState);
				expect(observedLoginStates).toEqual([
					null,
					expectedLoginState
				]);
			});

			it('should throw an exception on login failure', () => {
				//when
				let errorDescription = 'Wrong password';
				let caught: string;

				authService.login('Bob', 'bob').subscribe(
					result => {throw `login.subscribe() shouldn't succeed, but got: {${result}}`;},
					error => caught = error
				);
				connection.mockError({
					status: 400,
					_body: {
							error_: 'some_error',
							error_description: errorDescription,
					}
				} as any as Error);

				//then
				expect(caught).toEqual(errorDescription);
				expect(observedLoginStates).toEqual([null]);
			});
		});

		describe('register()', () => {
			it('should send a Register request', () => {
				//given
				let username = 'Bob', password = 'Bob\'s secret';

				//when
				authService.register(username, password).subscribe();

				//then
				expect(connection.request.json()).toEqual(
					{Name: username, Password: password}
				);
				expect(connection.request.url).toMatch('/Account/Register$');
			});

			it('should throw if registration fails', () => {
				//given
				let caught: string;
				authService.register('Harry', 'Harry\'s secret').subscribe(
					success => { throw `Should have thrown, but returned ${success}`; },
					e => caught = e
				);
				let message = 'Registration is closed.';

				//when
				connection.mockError({
					status: 400,
					_body: JSON.stringify({
						modelState: {'': [message]}
					})
				} as any as Error);

				//then
				expect(caught).toEqual(message);
			});
		});
  });
}
