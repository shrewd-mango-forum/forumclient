import { Injectable } from '@angular/core';
import { ForumApiService } from './forum-api.service';

import { Config } from '../config/env.config';
import { Observable } from 'rxjs/Observable';

export abstract class AccountService {
	public abstract checkRole(roleName: string): Observable<boolean>
}

@Injectable()
export class AccountServiceImpl extends AccountService {
	constructor(private api: ForumApiService) {
		super();
	}

	public checkRole(roleName: string) {
		return this.api.get(`${Config.API}/api/account/checkRole/${roleName}`)
			.map(response => response.json() === true);
	}
}
