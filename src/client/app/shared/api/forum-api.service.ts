import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptionsArgs, ResponseContentType } from '@angular/http';
import { AuthService } from './auth.service';

@Injectable()
export class ForumApiService {
	constructor(private http: Http, private auth: AuthService) {
	}

	get(url: string) {
		return this.http.get(url, this.requestOptions());
	}

	post(url: string, body: any) {
		return this.http.post(url, body, this.requestOptions());
	}

	delete(url: string) {
		return this.http.delete(url, this.requestOptions());
	}

	head(url: string) {
		return this.http.head(url);
	}

	private requestOptions(): RequestOptionsArgs {
		return {
			headers: new Headers({
				'Authorization' : this.authorizationHeader(),
				'Accept' : 'application/json',
			}),
			responseType: ResponseContentType.Json
		};
	}

	private authorizationHeader(): string {
		return this.auth.getLoginState()
			? `Bearer ${this.auth.getLoginState().token}`
			: null;
	}
}
