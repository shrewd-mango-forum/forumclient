import { Route } from '@angular/router';
import { ThreadComponent } from './thread.component';

export const threadRoutes: Route[] = [
	{
		path: 'thread/:threadId',
		component: ThreadComponent
	}
];
