import { Component, Input, Injectable, OnInit } from '@angular/core';
import { ThreadSummary } from '../models/index';
import { viewThreadCommands } from './thread.component';

@Component({
  moduleId: module.id,
  selector: 'thread-summary',
  templateUrl: 'thread-summary.component.html',
	styleUrls: ['thread-summary.component.css']
})
@Injectable()
export class ThreadSummaryComponent implements OnInit {
	@Input() thread: ThreadSummary;
	private viewThreadCommands: string[];

	ngOnInit() {
		this.viewThreadCommands = viewThreadCommands(this.thread);
	}
}
