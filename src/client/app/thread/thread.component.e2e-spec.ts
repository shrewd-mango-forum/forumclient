import {
	ApiHelper,
	AuthHelper,
	PathHelper,
	adminIdentity,
	loginSelectors,
	topicSelectors } from '../../e2e-helpers/';
import {
	PostTopicRequestModel,
	PostTopicResponseModel,
	PostThreadRequestModel,
	PostThreadResponseModel
} from '../models/';

const randomNumericString = (length: number) => Math.floor(Math.random() * Math.pow(10, length));

const threadSelectors = new class {
	const replyCreator = {
		replyButton() {return element(by.buttonText('Reply')); },
		textArea() {return element(by.css('reply-creator textArea.content-input')); },
		replyPreview() {return element(by.css('reply-creator .preview')); },
		previewButton() {return element(by.css('reply-creator')).element(by.buttonText('Preview')); },
		editButton() {return element(by.css('reply-creator')).element(by.buttonText('Edit')); },
		submitButton() {return element(by.css('reply-creator')).element(by.buttonText('Submit')); },
		cancelButton() {return element(by.css('reply-creator')).element(by.buttonText('Cancel')); },
	};
	const deleteThreadControls = {
		deleteButton() {return element(by.css('.btn-delete')); },
		cancelButton() {return element(by.css('.btn-delete-cancel')); },
		confirmButton() {return element(by.css('.btn-delete-confirm')); },
	};
	threadTitle() {return element(by.css('.thread-title')); }
	replies() {return element.all(by.css('.reply-content')); }
	topicLink() {return element(by.css('parent-topic a')); }
};

describe('Thread Viewer', () => {
	let topicRequest: PostTopicRequestModel;
	let topicResponse: PostTopicResponseModel;
	let threadRequest: PostThreadRequestModel;
	let threadResponse: PostThreadResponseModel;
	beforeEach(() => {
		const key = randomNumericString(6);
		topicRequest = {
			id: `test-topic-${key}`,
			name: `Test Topic ${key}`,
			description: 'Topic for testing'
		};
		ApiHelper.postTopic(`${PathHelper.apiLocation}/api/topic`, topicRequest)
		.then(resultTopic => {
			topicResponse = resultTopic;
			threadRequest = {
				title: 'TestThread',
				content: 'Test content',
				parentTopicId: topicRequest.id
			};
			return ApiHelper.postThread(resultTopic.actions.postThreadUrl, threadRequest);
		}).then(resultThread => {
				threadResponse = resultThread;
		});
	});

	afterEach(() => {
		ApiHelper.deleteTopic(topicResponse.actions.deleteTopicUrl);
	});

	it('can be navigated from the parent topic', () => {
		browser.get(`#/topic/${topicRequest.id}`);
		topicSelectors.threadLink(threadRequest.title).click();

		expect(browser.getCurrentUrl()).toContain(`/thread/${threadResponse.id}`);
		expect(threadSelectors.threadTitle().getText().then(text => text.trim()))
			.toEqual(threadRequest.title);
	});

	describe('when viewing thread', () => {
		beforeEach(() => {
			browser.get(`#/thread/${threadResponse.id}`);
		});

		it('shows thread title and thread content', () => {
			expect(threadSelectors.threadTitle().getText().then(text => text.trim()))
				.toEqual(threadRequest.title);
			expect(threadSelectors.replies().map(e => e.getText()))
				.toEqual([threadRequest.content]);
		});

		it('does not have a reply button while not logged in', () => {
			expect(threadSelectors.replyCreator.replyButton().isPresent())
				.toBeFalsy();
		});

		it('allows navigation back to the thread\'s topic', () => {
			expect(threadSelectors.topicLink().getText())
				.toEqual(topicRequest.name);

			threadSelectors.topicLink().click();
			expect(topicSelectors.topicName().getText()).toEqual(topicRequest.name);
		});

		it('does not show the Delete Thread button', () => {
			expect(threadSelectors.deleteThreadControls.deleteButton().isPresent())
				.toBeFalsy();
		});

		describe('after logging in as a regular user and clicking Reply button', () => {
			const user = {
				username: `User${randomNumericString(5)}`,
				password: 'my-password'
			};
			const replyContent = 'What an interesting topic.';
			beforeAll(() => {
				AuthHelper.registerNewUser(user);
			});
			beforeEach(() => {
				loginSelectors.usernameInput().sendKeys(user.username);
				loginSelectors.passwordInput().sendKeys(user.password);
				loginSelectors.loginButton().click();
				browser.sleep(50);
				threadSelectors.replyCreator.replyButton().click();
			});

			it('allows me to type reply, preview, and submit', () => {
				threadSelectors.replyCreator.textArea().sendKeys(replyContent);
				threadSelectors.replyCreator.previewButton().click();
				expect(threadSelectors.replyCreator.replyPreview().getText())
					.toBe(replyContent);

				threadSelectors.replyCreator.submitButton().click();
				expect(threadSelectors.replies().map(e => e.getText()))
					.toContain(replyContent);
			});

			it('should disable "Preview" button whenever content is invalid', () => {
				expect(threadSelectors.replyCreator.previewButton().isEnabled()).toBe(false);
				threadSelectors.replyCreator.textArea().sendKeys(replyContent);
				expect(threadSelectors.replyCreator.previewButton().isEnabled()).toBe(true);
				threadSelectors.replyCreator.textArea().clear();
				threadSelectors.replyCreator.textArea().sendKeys(' ', protractor.Key.BACK_SPACE); // :(
				expect(threadSelectors.replyCreator.previewButton().isEnabled()).toBe(false);
			});
		});

		describe('after logging in as an admin', () => {
			beforeEach(() => {
				loginSelectors.usernameInput().sendKeys(adminIdentity.username);
				loginSelectors.passwordInput().sendKeys(adminIdentity.password);
				loginSelectors.loginButton().click();
				browser.sleep(10);
			});

			describe('deleting the thread', () => {
				beforeEach(() => {
					threadSelectors.deleteThreadControls.deleteButton().click();
				});

				it('allows me to cancel deleting the thread', () => {
					threadSelectors.deleteThreadControls.cancelButton().click();
					expect(threadSelectors.deleteThreadControls.confirmButton().isPresent()).toBeFalsy();
				});

				it('allows me to delete the thread, returning to the parent topic', () => {
					//when
					threadSelectors.deleteThreadControls.confirmButton().click();

					//then
					expect(browser.getCurrentUrl()).toMatch(`/topic/${topicRequest.id}`);
					expect(topicSelectors.topicName().getText()).toEqual(topicRequest.name);
					expect(topicSelectors.threadLink(threadRequest.title).isPresent()).toBeFalsy();
				});
			});
		});
	});
});
