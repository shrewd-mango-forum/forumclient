import { Component, EventEmitter, Input, Injectable, OnInit, Output, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { GetThreadResponseModel } from '../models/index';
import { ThreadService } from '../shared/api/index';

import { Subscription } from 'rxjs/Subscription';

@Component({
  moduleId: module.id,
  selector: 'reply-creator',
  templateUrl: 'reply-creator.component.html',
	styleUrls: ['reply-creator.component.css']
})
@Injectable()
export class ReplyCreatorComponent implements OnInit, OnDestroy {
	@Input() parentThread: GetThreadResponseModel;
	@Output() newReply = new EventEmitter<void>();
	@Output() refresh = new EventEmitter<void>();
	@Output() close = new EventEmitter<void>();
	form: FormGroup;

	formErrors: {[id: string] : string[] } = {
		content: []
	};

	validationMessages: {[fieldName: string] : {[validatorKey: string] : string}} = {
		content: {'required' : 'Reply content is required.'}
	};

	previewing: boolean = false;
	pending: boolean = false;
	subscription: Subscription = new Subscription();
	error?: String;

	constructor(private formBuilder: FormBuilder, private threadService: ThreadService ) {
	}

	ngOnInit() {
		this.form = this.formBuilder.group({
			'content' : ['', [Validators.required]]
		});
		this.form.statusChanges.subscribe(data => this.onStatusChanged(data));
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

	onStatusChanged(data: any) {
		if (!this.form) {
			return;
		}

		for (const field in this.formErrors) {
			this.formErrors[field] = null;
			const control = this.form.get(field);
			if (control && control.dirty && !control.valid) {
				this.formErrors[field] = Object.keys(control.errors)
					.map(key => this.validationMessages[field][key]);
			}
		}
	}

	onPreview() {
		this.previewing = true;
	}

	onEdit() {
		this.previewing = false;
	}

	onCancel() {
		this.close.next();
	}

	onSubmit() {
		this.error = null;
		this.pending = true;
		const value = this.form.value;
		this.subscription = this.threadService.postReply({
			reply: {content: value.content},
			thread: this.parentThread
		}).subscribe(
			result => {
				this.newReply.next();
			},
			error => {
				this.error = 'Failed to post the reply.';
				this.pending = false;
				this.previewing = false;
				this.refresh.next();
			}
		);
	}
}
