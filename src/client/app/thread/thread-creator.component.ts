import { Component, EventEmitter, Input, Injectable, OnInit, Output, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { GetTopicResponseModel } from '../models/topicModels';
import { PostThreadResponseModel } from '../models/threadModels';
import { ThreadService } from '../shared/api/index';

import { Subscription } from 'rxjs/Subscription';

@Component({
  moduleId: module.id,
  selector: 'thread-creator',
  templateUrl: 'thread-creator.component.html'
})
@Injectable()
export class ThreadCreatorComponent implements OnInit, OnDestroy {
	@Input() parentTopic: GetTopicResponseModel;
	@Output() newThread = new EventEmitter<PostThreadResponseModel>();
	@Output() cancel = new EventEmitter<void>();
	form: FormGroup;

	formErrors: {[id: string] : string[] } = {
		title: [],
		content: []
	};

	validationMessages: {[fieldName: string] : {[validatorKey: string] : string}} = {
		title: {'required' : 'Thread title is required.'},
		content: {'required' : 'Thread content is required.'}
	};

	pending: boolean;
	subscription: Subscription = new Subscription();;
	message: String;

	constructor(private formBuilder: FormBuilder, private threadService: ThreadService ) {
	}

	ngOnInit() {
		this.form = this.formBuilder.group({
			'title': ['', [Validators.required]],
			'content' : ['', [Validators.required]]
		});
		this.form.statusChanges.subscribe(data => this.onStatusChanged(data));
		this.pending = false;
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

	onStatusChanged(data: any) {
		if (!this.form) {
			return;
		}

		for (const field in this.formErrors) {
			this.formErrors[field] = null;
			const control = this.form.get(field);
			if (control && control.dirty && !control.valid) {
				this.formErrors[field] = Object.keys(control.errors)
					.map(key => this.validationMessages[field][key]);
			}
		}
	}

	onSubmit() {
		this.pending = true;
		const value = this.form.value;
		this.subscription = this.threadService.postThread({
			title: value.title,
			content: value.content,
			parent: this.parentTopic
		}).subscribe(
			result => this.newThread.emit(result)
		);
	}

	onCancel() {
		this.cancel.emit();
	}
}
