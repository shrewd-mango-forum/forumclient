import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import {
	async,
	fakeAsync,
	tick,
	ComponentFixture,
	TestBed,
} from '@angular/core/testing';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import { MockableServiceMethod } from '../../test-helpers/index';
import { GetThreadResponseModel, ThreadSummary, TopicSummary } from '../models/index';
import { AuthService, ThreadService } from '../shared/api/index';
import { Config } from '../shared/config/env.config';
import { threadRoutes } from './thread.routes';
import { ThreadComponent, viewThreadCommands } from './thread.component';

export function main() {
	describe('Thread component', () => {

		class MockThreadServiceBackend {
			public service = {
				getThread: (url: string) => this._get.request(url),
				deleteThread: (url: string) => this._delete.request(url),
			};

			private _get = new MockableServiceMethod<GetThreadResponseModel>('GET');
			private _delete = new MockableServiceMethod<void>('DELETE');

			public resolveGetRequest = (url: string, value: GetThreadResponseModel) => this._get.resolveRequest(url, value);
			public resolveDeleteRequest = (url: string) => this._delete.resolveRequest(url, undefined);

			public hasOutstandingRequests = () => this._get.hasOutstandingRequests()
				&& this._delete.hasOutstandingRequests();
		}

		let mockThreadService: MockThreadServiceBackend;

		let mockAuthService: {
			service: {
				getLoginStateObservable(): Observable<void>;
			}
			emitLoginState(): void
		};

		const mockThread : GetThreadResponseModel = {
			title: 'Toasters with buttons?',
			author: 'bob',
			id: 'thread1',
			topic: {
				id: 'toasters',
				name: 'Toasters',
				getTopicUrl: '/topic/toasters',
				description: 'dicussion about toasters'
			},
			replies: [
				{
					author: 'bob',
					date: new Date(),
					content: 'Do you guys like toasters with buttons? I find them a bit impersonal.'
				},
				{
					author: 'strudeloo',
					date: new Date(),
					content: 'maybe for settings but it absolutely must have a lever'
				}
			],
			actions: {
				getUrl: `${Config.API}/api/thread/thread1`,
				replyUrl: `${Config.API}/api/thread/thread1?reply`,
				deleteUrl: `${Config.API}/api/thread/thread1?delete`,
			},
		};

		beforeEach((done) => {
			mockThreadService = new MockThreadServiceBackend();

			const authServiceObservable = new BehaviorSubject(undefined);
			mockAuthService = {
				service: {
					getLoginStateObservable(): Observable<void> {
						return authServiceObservable.asObservable();
					}
				},
				emitLoginState() {
					authServiceObservable.next(undefined);
				}
			};

			TestBed
				.configureTestingModule({
						declarations: [
							TestComponent,
							ThreadComponent,
							MockReplyContentDisplayComponent,
							MockReplyCreatorComponent,
							MockParentTopicComponent,
							MockTopicComponent
						],
						imports: [
							RouterTestingModule.withRoutes([
								...threadRoutes,
								{
									path: 'topic/:topicId',
									component: MockTopicComponent
								}])
						],
						providers: [
							{
								provide: ThreadService,
								useValue: mockThreadService.service
							},
							{
								provide: AuthService,
								useValue: mockAuthService.service
							},
						]
				})
			.compileComponents().then(done);
		});

		describe('viewing a thread', () => {
			let fixture: ComponentFixture<TestComponent>;
			const selectors = {
				threadDetails: () => fixture.nativeElement.querySelector('.thread-details'),
				replyDetails: (replyIndex: number) => fixture.nativeElement.querySelectorAll('.reply')[replyIndex].querySelector('.reply-details'),
				replyContent: (replyIndex: number) => fixture.nativeElement.querySelectorAll('.reply')[replyIndex].querySelector('.reply-content'),
				replyButton: () => fixture.nativeElement.querySelector('.reply-button'),
				deleteButton: () => fixture.nativeElement.querySelector('.btn-delete'),
				deleteControls: {
					cancelButton: () => fixture.nativeElement.querySelector('.btn-delete-cancel'),
					confirmButton: () => fixture.nativeElement.querySelector('.btn-delete-confirm')
				}
			};
			let router: Router;
			const mockThreadSummary: ThreadSummary = {
				title: mockThread.title,
				id: mockThread.id,
				threadUrl: mockThread.actions.getUrl
			};

			beforeEach(fakeAsync(() => {
				fixture = TestBed.createComponent(TestComponent);
				router = TestBed.get(Router) as Router;

				router.navigate(viewThreadCommands(mockThreadSummary));
				tick();
				mockThreadService.resolveGetRequest(mockThread.actions.getUrl, mockThread);
				fixture.detectChanges();
			}));

			afterEach(() => {
				expect(mockThreadService.hasOutstandingRequests()).toBeFalsy();
			});

			it('should display details about the thread', () => {
				expect(fixture.nativeElement.querySelector('h1').textContent).toEqual(mockThread.title);
				expect(selectors.threadDetails().textContent.trim()).toBe(mockThread.topic.name);
			});

			it('should refresh when the login state changes', async(() => {
				mockAuthService.emitLoginState();
				mockThreadService.resolveGetRequest(mockThread.actions.getUrl, mockThread);
			}));

			it('should show every reply', () => {
				for (let i = 0; i < mockThread.replies.length; i++) {
					const reply = mockThread.replies[i];
					expect(selectors.replyDetails(i).textContent).toContain(reply.author);
					expect(selectors.replyDetails(i).textContent)
						.toContain(reply.date.toLocaleDateString());
					expect(selectors.replyContent(i).textContent.trim()).toBe(reply.content);
				}
			});

			it('should show the Reply button only when the user can reply', () => {
				expect(selectors.replyButton()).toBeTruthy();

				mockAuthService.emitLoginState();
				mockThreadService.resolveGetRequest(mockThread.actions.getUrl, Object.assign({},
					mockThread,
					{actions: Object.assign({},
						mockThread.actions,
						{replyUrl: null}
					)}
				));
				fixture.detectChanges();
				expect(selectors.replyButton()).toBeFalsy();

				mockAuthService.emitLoginState();
				mockThreadService.resolveGetRequest(mockThread.actions.getUrl, mockThread);
				fixture.detectChanges();
				expect(selectors.replyButton()).toBeTruthy();
			});

			it('should show the Delete button only when the user can delete', () => {
				expect(selectors.deleteButton()).toBeTruthy();

				mockAuthService.emitLoginState();
				mockThreadService.resolveGetRequest(mockThread.actions.getUrl, Object.assign({},
					mockThread,
					{actions: Object.assign({},
						mockThread.actions,
						{deleteUrl: null}
					)}
				));
				fixture.detectChanges();
				expect(selectors.deleteButton()).toBeFalsy();

				mockAuthService.emitLoginState();
				mockThreadService.resolveGetRequest(mockThread.actions.getUrl, mockThread);
				fixture.detectChanges();
				expect(selectors.deleteButton()).toBeTruthy();
			});

			describe('deleting a thread', () => {
				beforeEach(() => {
					fixture.autoDetectChanges(true);
					selectors.deleteButton().click();
				});

				it('no longer shows the regular delete button', () => {
					expect(selectors.deleteButton()).toBeFalsy();
				});

				it('can be cancelled with the cancel button', () => {
					selectors.deleteControls.cancelButton().click();

					expect(selectors.deleteButton()).toBeTruthy();
					expect(selectors.deleteControls.confirmButton()).toBeFalsy();
				});

				it('automatically cancelled by starting a reply', () => {
					selectors.replyButton().click();

					expect(selectors.deleteButton()).toBeTruthy();
					expect(selectors.deleteControls.confirmButton()).toBeFalsy();
				});

				it('sends the Delete request, then loads the parent topic', fakeAsync(() => {
					selectors.deleteControls.confirmButton().click();

					mockThreadService.resolveDeleteRequest(mockThread.actions.deleteUrl);

					tick();
					expect(router.url)
						.toEqual(`/topic/${mockThread.topic.id}`);
				}));
			});

			describe('after user clicks "Reply" and then the thread is refreshed', () => {
				beforeEach(() => {
					fixture.autoDetectChanges(true);
					fixture.nativeElement.querySelector('.reply-button').click();
					mockThreadService.resolveGetRequest(mockThread.actions.getUrl, mockThread);
				});

				it('passes the thread down to the reply-creator', () => {
					expect(fixture.nativeElement.querySelector('reply-creator .threadReplyUrl').textContent)
						.toEqual(mockThread.actions.replyUrl);
				});

				it('refreshes the thread if the reply-creator requests it', () => {
					fixture.nativeElement.querySelector('reply-creator .refresh').click();
					mockThreadService.resolveGetRequest(mockThread.actions.getUrl, mockThread);
				});

				it('closes the reply-creator without a refresh', () => {
					fixture.nativeElement.querySelector('reply-creator .close').click();
					expect(fixture.nativeElement.querySelector('reply-creator')).toBeFalsy();
				});

				it('closes the reply-creator and refreshes when a new reply is emitted', () => {
					fixture.nativeElement.querySelector('reply-creator .new-reply').click();
					expect(fixture.nativeElement.querySelector('reply-creator')).toBeFalsy();
					mockThreadService.resolveGetRequest(mockThread.actions.getUrl, mockThread);
				});

				it('automatically closes the reply-creator if delete button is clicked', () => {
					selectors.deleteButton().click();

					expect(fixture.nativeElement.querySelector('reply-creator')).toBeFalsy();
				});
			});
		});
	});
}

@Component({
	selector: 'test-cmp',
	template: '<router-outlet></router-outlet>'
})
class TestComponent { }

@Component({
	selector: 'reply-creator',
	template: `
		<div class="threadReplyUrl">{{parentThread.actions.replyUrl}}</div>
		<button (click)="onNewReply()" class="new-reply">New Reply</button>
		<button (click)="onRefresh()" class="refresh">Refresh</button>
		<button (click)="onClose()" class="close">Close</button>
	`
})
class MockReplyCreatorComponent {
	@Input() parentThread: GetThreadResponseModel;
	@Output() newReply = new EventEmitter<void>();
	@Output() refresh = new EventEmitter<void>();
	@Output() close = new EventEmitter<void>();
	onNewReply() {
		this.newReply.next();
	}
	onClose() {
		this.close.next();
	}
	onRefresh() {
		this.refresh.next();
	}
}

@Component({
	selector: 'reply-content-display',
	template: '<ng-content></ng-content>'
})
class MockReplyContentDisplayComponent { }

@Component({
	selector: 'parent-topic',
	template: '{{topic.name}}'
})
class MockParentTopicComponent {
	@Input() topic: TopicSummary;
}

@Component({
	template: ''
})
class MockTopicComponent {
}
