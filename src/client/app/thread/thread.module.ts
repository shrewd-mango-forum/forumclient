import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { ReplyContentDisplayComponent } from './reply-content-display.component';
import { ReplyCreatorComponent } from './reply-creator.component';

import { ApiModule } from '../shared/api/index';
import { SharedComponentsModule } from '../shared/shared-components/index';

import { ThreadComponent } from './thread.component';
import { ThreadCreatorComponent } from './thread-creator.component';
import { ThreadSummaryComponent } from './thread-summary.component';

@NgModule({
    imports: [ApiModule, CommonModule, ReactiveFormsModule, RouterModule, SharedComponentsModule],
    declarations: [ReplyContentDisplayComponent, ReplyCreatorComponent, ThreadComponent, ThreadCreatorComponent, ThreadSummaryComponent],
    exports: [ThreadCreatorComponent, ThreadSummaryComponent]
})

export class ThreadModule { }
