import { Component, Injectable, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';

import {
	GetThreadResponseModel,
	ThreadSummary,
	threadIdToThreadUrl,
} from '../models/index';
import { AuthService, ThreadService } from '../shared/api/index';
import { topicCommands } from '../topic/index';

enum threadComponentState {
	normal,
	replying,
	deleting
}

export function viewThreadCommands(thread: ThreadSummary) {
	return ['/thread/', thread.id];
};

@Component({
	moduleId: module.id,
	templateUrl: 'thread.component.html',
	styleUrls: ['thread.component.css']
})
@Injectable()
export class ThreadComponent implements OnInit, OnDestroy {
	public threadComponentState = threadComponentState;
	getThreadUrl: string;
	thread: GetThreadResponseModel;
	error = false;
	state = threadComponentState.normal;
	authSubscription: Subscription = new Subscription();
	constructor(
		private activatedRoute: ActivatedRoute,
		private authService: AuthService,
		private threadService: ThreadService,
		private router: Router) {
			this.activatedRoute.params.forEach(param => {
				this.getThreadUrl = param['threadId']
				? threadIdToThreadUrl(param['threadId'])
				: null;
			});
	}

	ngOnInit() {
		this.refresh();
	}

	setStateNormal() {
		this.state = threadComponentState.normal;
	}

	setStateReplying() {
		this.state = threadComponentState.replying;
		this.refresh();
	}

	setStateDeleting() {
		this.state = threadComponentState.deleting;
	}

	finishReply() {
		this.state = threadComponentState.normal;
		this.refresh();
	}

	deleteThread() {
		this.threadService.deleteThread(this.thread.actions.deleteUrl)
			.subscribe(_ => {
				this.router.navigate(topicCommands(this.thread.topic));
			});
	}

	refresh() {
		this.authSubscription.unsubscribe();
		this.authSubscription = this.authService.getLoginStateObservable()
			.switchMap(_ => this.threadService.getThread(this.getThreadUrl))
			.subscribe(
				thread => {this.thread = thread; this.error = false; },
				error => {this.thread = null; this.error = true; }
			);
	}

	ngOnDestroy() {
		this.authSubscription.unsubscribe();
	}
}
