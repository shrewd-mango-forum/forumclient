import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TestBed } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import {
	async
} from '@angular/core/testing';
import {
	Route
} from '@angular/router';
import {
	RouterTestingModule
} from '@angular/router/testing';
import { AppComponent } from './app.component';
import { ApiModule, AuthService, LoginState } from './shared/api/index';
import { SharedModule } from './shared/shared.module';
import { TopicComponent } from './topic/topic.component';
import { TopicModule } from './topic/topic.module';

export function main() {

	describe('App component', () => {

		let config: Route[] = [
			{ path: '', component: TopicComponent }
		];

		let authService = {
			getLoginStateObservable(): Observable<LoginState> {
				return new Observable<LoginState>(() => new Subscription());
			}
		};

		beforeEach(() => {
			TestBed.configureTestingModule({
				imports: [ApiModule, FormsModule, RouterTestingModule.withRoutes(config), SharedModule, TopicModule],
				declarations: [TestComponent, AppComponent],
				providers: [
					{ provide: APP_BASE_HREF, useValue: '/' },
					{ provide: AuthService, useValue: authService }
				]
			});
		});

		it('should build without a problem',
			async(() => {
				TestBed
					.compileComponents()
					.then(() => {
						let fixture = TestBed.createComponent(TestComponent);
						let compiled = fixture.nativeElement;

						expect(compiled).toBeTruthy();
					});
			}));
	});
}

@Component({
	selector: 'test-cmp',
	template: '<sd-app></sd-app>'
})

class TestComponent {
}



