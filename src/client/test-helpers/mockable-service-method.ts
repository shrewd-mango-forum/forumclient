import { Observable } from 'rxjs/Observable';
import { Subscriber } from 'rxjs/Subscriber';

export class MockableServiceMethod<OutputType> {
	private pending: {[url: string]: Subscriber<OutputType>} = {};

	constructor(private name: string) { }

	public resolveRequest = (url: string, value: OutputType) => {
		if (!this.pending[url]) {
			throw new Error(`
			Mock service error: No pending request for ${this.name} ${url}.
			Actual pending requests: [${Object.keys(this.pending).join(', ')}].
			`);
		}
		this.pending[url].next(value);
		this.pending[url].complete();
		delete this.pending[url];
	}

	public failRequest = (url: string, error: any) => {
		if (!this.pending[url]) {
			throw new Error(`
				Mock service error: No pending request for ${this.name} ${url}.
				Actual pending requests: [${Object.keys(this.pending).join(', ')}].
				`);
		}
		this.pending[url].error(error);
		delete this.pending[url];
	}

	public request(url: string): Observable<OutputType> {
			return new Observable<OutputType>((subscriber: Subscriber<OutputType>) => {
				if (this.pending[url]) {
					throw new Error(`Mock service error: duplicate subscription for ${this.name} ${url}.`);
				}
				this.pending[url] = subscriber;
				return {
					unsubscribe() {
						if (url in pending) {
							//should be resolved, or implement a way to expect cancellation explicitly.
							throw new Error(`Mock service error: trying to unsubscribe for ${this.name} ${url}.`);
						}
					}
				};
			});
	}

	public hasOutstandingRequests() {
		return Object.keys(this.pending).length > 0;
	}
}
