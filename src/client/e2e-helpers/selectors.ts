export const loginSelectors = new class {
	usernameInput() {
		return element(by.css('.login-form input.username-input'));
	}
	passwordInput() {
		return element(by.css('.login-form input.password-input'));
	}
	loginButton() {
		return element(by.buttonText('Sign In'));
	}
	newUserButton() {
		return element(by.buttonText('New User?'));
	}
	logoutButton() {
		return element(by.buttonText('Sign Out'));
	}
	alert() {
		return element(by.css('.login-form .alert'));
	}
};

export const registerSelectors = new class {
	usernameInput() {
		return element(by.css('forum-register-form input.username-input'));
	}
	passwordInput() {
		return element(by.css('forum-register-form input.password-input'));
	}
	confirmPasswordInput() {
		return element(by.css('forum-register-form input.confirm-password-input'));
	}
	registerButton() {
		return element(by.buttonText('Join'));
	}
	closeButton() {
		return element(by.buttonText('Existing User?'));
	}
	validationMessages() {
		return element.all(by.css('forum-register-form .validation-message'));
	}
	alert() {
		return element(by.css('forum-register-form .alert'));
	}
};

export const topicSelectors = new class {
	topicName() {
		return element(by.css('.topic-name'));
	}

	topicDescription() {
		return element(by.css('.topic-description'));
	}

	parentTopicLink() {
		return element(by.css('parent-topic a'));
	}
	error() {
		return element(by.css('.error'));
	}

	childTopicLink(name: string) {
		return element.all(by.cssContainingText('.child-topic', name))
			.first()
			.element(by.css('a'));
	}

	threadLink(threadTitle: string) {
		return element(by.cssContainingText('thread-summary a', threadTitle));
	}

	deleteTopicButton() {
		return element(by.css('.btn-delete-topic'));
	}

	confirmDeleteTopicButton() {
		return element(by.css('.confirm-delete-topic .btn-confirm'));
	}
};
