import * as request from 'request';
import { IncomingMessage }  from 'http';
import Promise = webdriver.promise.Promise;

import { AuthHelper, adminIdentity } from './auth-helper';
import {
	GetTopicResponseModel,
	PostThreadRequestModel,
	PostThreadResponseModel,
	PostTopicRequestModel,
	PostTopicResponseModel
} from '../app/models/';

export class ApiHelper {
	static postTopic(url: string, topic: PostTopicRequestModel): Promise<PostTopicResponseModel> {
		return this.requestAsAdmin(url, topic, 'post');
	};

	static deleteTopic(url: string): Promise<void> {
		return this.requestAsAdmin(url, undefined, 'delete').then(_ => undefined);
	}

	static getTopic(url: string): Promise<GetTopicResponseModel> {
		return this.requestAsAdmin(url, null, 'get');
	};

	static postThread(url: string, thread: PostThreadRequestModel): Promise<PostThreadResponseModel> {
		return this.requestAsAdmin(url, thread, 'post');
	}

	private static requestAsAdmin<RequestType, ResponseType>(url: string, body: RequestType, method: string): Promise<ResponseType> {
		return AuthHelper.getToken(adminIdentity).then(token =>
			this.executeRequest({
				url: url,
				headers: {authorization: `Bearer ${token}`},
				body: body,
				json: true,
				method: method
			}).then(result => {
				try {
					return result;
				} catch (e) {
					console.error(result);
					throw e;
				}
			})
		);
	}

	private static executeRequest<T>(requestObject: request.Options): Promise<T> {
		return protractor.promise.controlFlow().execute(() => {
				const deferred = protractor.promise.defer();
				request(
						requestObject,
						(e: any, i: IncomingMessage, r: string) => {
								if (e) {
										deferred.reject(JSON.stringify(e));
								} else if (i.statusCode < 200 || i.statusCode >=300) {
										deferred.reject(JSON.stringify(i));
								} else {
										deferred.fulfill(r);
								}
						});
				return deferred.promise;
		});
	}
}
