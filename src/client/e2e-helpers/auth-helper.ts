import { PathHelper } from './path-helper';
import { IncomingMessage }  from 'http';
import * as request from 'request';
import Promise = webdriver.promise.Promise;

const registerUrl : string = `${PathHelper.apiLocation}/api/Account/Register`;
const tokenUrl : string = `${PathHelper.apiLocation}/Token`;

export class Identity {
	username: string;
	password: string;
}

export const adminIdentity: Identity = {
	username: 'Admin',
	password: 'MySecret'
};

export class AuthHelper {
    static registerNewUser(identity: Identity) : Promise<void> {
        let requestObject = {
            url: registerUrl,
            body: {Name: identity.username, Password: identity.password},
            json: true
        };
        return this.executeRequest(requestObject)
            .then(function(_) { return; });
    }

    static getToken(identity: Identity) : Promise<string> {
        let requestObject = {
            url: tokenUrl,
            form: {
                grant_type: 'password',
                username: identity.username,
                password: identity.password
            }
        };

        return this.executeRequest(requestObject)
            .then((result) => JSON.parse(result)['access_token']);
    }

    private static executeRequest(requestObject: request.Options): Promise<string> {
        return protractor.promise.controlFlow().execute(() => {
            let deferred = protractor.promise.defer();
            request.post(
                requestObject,
                (e: any, i: IncomingMessage, r: string) => {
                    if (e) {
                        deferred.reject(JSON.stringify(e));
                    } else if (i.statusCode < 200 || i.statusCode >=300) {
                        deferred.reject(JSON.stringify(i));
                    } else {
                        deferred.fulfill(r);
                    }
                });
            return deferred.promise;
        });
    }
}
