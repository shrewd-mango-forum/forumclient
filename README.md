# Client for Shrewd Mango Forum

This is an angular2 client for the corresponding backend at https://bitbucket.org/shrewd-mango-forum/forum/src
It's forked from the angular2 seed at https://github.com/mgechev/angular-seed

# How to start
* Update tools/env/base.ts to point to a forum backend
```bash
$ npm install
$ npm start
```

# License

MIT